// Copyright 2015 Pascal Krabbe

#include "AdvKitEditor.h"
#include "SlateStyle.h"

#include "AdvKitTraceComponentVisualizer.h"
#include "Player/AdvKitTraceUtilityComponent.h"


/**
 * @brief Editor module to register custom visualizer for UAdvKitTraceUtilityComponent.
 */
class FAdvKitEditor : public IModuleInterface
{
	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};

IMPLEMENT_MODULE(FAdvKitEditor, AdvKitEditor)


void FAdvKitEditor::StartupModule()
{
	if (GUnrealEd != NULL)
	{
		TSharedPtr<FComponentVisualizer> Visualizer = MakeShareable(new FAdvKitTraceComponentVisualizer());

		if (Visualizer.IsValid())
		{
			GUnrealEd->RegisterComponentVisualizer(UAdvKitTraceUtilityComponent::StaticClass()->GetFName(), Visualizer);
			Visualizer->OnRegister();
		}
	}
}


void FAdvKitEditor::ShutdownModule()
{
	if (GUnrealEd != NULL)
	{
		GUnrealEd->UnregisterComponentVisualizer(UAdvKitTraceUtilityComponent::StaticClass()->GetFName());
	}
}



