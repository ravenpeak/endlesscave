// Copyright 2015 Pascal Krabbe

#pragma once

#include "Environment/AdvKitManualTransition.h"
#include "AdvKitManualTransitionArea.generated.h"

class AAdvKitCharacter;

/**
 * @brief Implementation for a manual area transition.
 */
UCLASS()
class ADVKITRUNTIME_API UAdvKitManualTransitionArea : public UAdvKitManualTransition
{
	GENERATED_BODY()
	
protected:
	/** Left start of the transition in local space of the source zone */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Config")
	FVector LocalSourceMin;

	/** Right start of the transition in local space of the source zone */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Config")
	FVector LocalSourceMax;

	/** Constrains the source locations to the zone before creating a transition */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Config")
	bool bSnapSource;

	/** Left end of the transition in local space of the target zone (or source if no target is given) */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Config")
	FVector LocalTargetMin;

	/** Right end of the transition in local space of the target zone (or source if no target is given) */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Config")
	FVector LocalTargetMax;

	/** Constrains the target locations to the zone before creating a transition */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Config")
	bool bSnapTarget;

public:
	//Begin UAdvKitManualTransition Interface
	virtual void CreateTransition() override;
	//End UAdvKitManualTransition Interface
};
