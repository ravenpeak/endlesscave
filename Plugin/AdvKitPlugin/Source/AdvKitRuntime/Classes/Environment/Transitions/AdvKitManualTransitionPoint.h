// Copyright 2015 Pascal Krabbe

#pragma once

#include "Environment/AdvKitManualTransition.h"
#include "AdvKitManualTransitionPoint.generated.h"

class AAdvKitCharacter;

/**
 * @brief Implementation for a manual point transition.
 */
UCLASS()
class ADVKITRUNTIME_API UAdvKitManualTransitionPoint : public UAdvKitManualTransition
{
	GENERATED_BODY()
	
protected:
	/** Start of the transition in local space of the source zone */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Config")
	FVector LocalSource;

	/** Constrains the source location to the zone before creating a transition */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Config")
	bool bSnapSource;

	/** End of the transition in local space of the target zone (or source if no target is given) */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Config")
	FVector LocalTarget;

	/** Constrains the target location to the zone before creating a transition */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Config")
	bool bSnapTarget;

public:

	//Begin UAdvKitManualTransition Interface
	virtual void CreateTransition() override;
	//End UAdvKitManualTransition Interface
};
