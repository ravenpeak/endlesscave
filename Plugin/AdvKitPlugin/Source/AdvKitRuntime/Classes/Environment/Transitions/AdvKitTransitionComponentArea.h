// Copyright 2015 Pascal Krabbe

#pragma once

#include "Environment/AdvKitTransitionComponent.h"
#include "AdvKitTransitionComponentArea.generated.h"

/**
 * @brief Transition that is an area between two defined lines on the source and target zone. 
 */
UCLASS()
class ADVKITRUNTIME_API UAdvKitTransitionComponentArea : public UAdvKitTransitionComponent
{
	GENERATED_BODY()

protected:
	/** In case the transition is dynamic, it is only valid if the line points have this distance to each other. */
	UPROPERTY(EditAnywhere, BlueprintReadonly, Category = "Transition")
	float DynamicTransitionLength;


	/** Left start of the transition line(inside the source zone) */
	UPROPERTY(BlueprintReadonly, Category = "Transition")
	FBasedPosition MinSourcePosition;

	/** Left end of the transition line (inside of the target zone) */
	UPROPERTY(BlueprintReadonly, Category = "Transition")
	FBasedPosition MinTargetPosition;

	/** Right start of the transition (inside the source zone) */
	UPROPERTY(BlueprintReadonly, Category = "Transition")
	FBasedPosition MaxSourcePosition;

	/** Right end of the transition (inside of the target zone) */
	UPROPERTY(BlueprintReadonly, Category = "Transition")
	FBasedPosition MaxTargetPosition;

#if WITH_EDITORONLY_DATA
protected:
	/** Units between in editor visualization arrows */
	static float ArrowDistanceUnits;

	/** In editor visualization arrows */
	UPROPERTY()
	TArray<	TWeakObjectPtr<class UArrowComponent> > Arrows;

public:
	/** Removes all visualization arrows */
	void ClearArrows();

	/**
	 * Adds a new visualization arrow
	 * @param	WorldStart 	Start point of the arrow
	 * @param	WorldTarget 	Target to point the arrow at
	 * @param	OptionalBase 	Base actor of the component
	 */
	void AddArrow(const FVector& WorldStart, const FVector& WorldTarget, AActor* OptionalBase = NULL);
#endif


public:
	/**
	 * Initializes the transition
	 * @param	ForCharacter 	Character that can use the transition
	 * @param	MinWorldSource 	Left start of the transition line on the source zone in world space
	 * @param	MinWorldTarget 	Left end of the transition line on the target zone in world space
	 * @param	MaxWorldSource 	Right start of the transition line on the source zone in world space
	 * @param	MaxWorldTarget 	Right end of the transition line on the target zone in world space
	 * @param	NewTargetPhysics 	Movement mode the character will have after the transition
	 * @param	TargetZone 		Zone the character will be in after the transition
	 * @param	NewTargetCustomPhysics	Custom movement mode the character will have after the transition
	 */
	virtual void Init(TSubclassOf<AAdvKitCharacter> ForCharacter, const FVector& MinWorldSource, const FVector& MinWorldTarget, const FVector& MaxWorldSource, const FVector& MaxWorldTarget, EAdvKitMovementMode NewTargetPhysics, class AAdvKitZone* TargetZone = NULL, uint8 NewTargetCustomPhysics = 0);

	//Begin UActorComponent Interface
	virtual void OnComponentDestroyed() override;
	//End UActorComponent Interface

	//Begin UAdvKitTransitionComponent Interface
	virtual bool IsDynamicTransitionValid() override;
	virtual FVector GetClosestSourcePosition(const FVector& ToWorldPosition) override;
	virtual FVector GetClosestTargetPosition(const FVector& ToWorldPosition) override;
	//End UAdvKitTransitionComponent Interface
};
