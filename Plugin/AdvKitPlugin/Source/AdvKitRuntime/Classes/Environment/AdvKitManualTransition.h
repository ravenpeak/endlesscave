// Copyright 2015 Pascal Krabbe

#pragma once

#include "Object.h"
#include "AdvKitTypes.h"
#include "AdvKitManualTransition.generated.h"

class AAdvKitCharacter;

/**
 * @brief Base class for objects to manually configure transitions on a zone.
 */
UCLASS(Blueprintable, abstract, EditInlineNew)
class ADVKITRUNTIME_API UAdvKitManualTransition : public UObject
{
	GENERATED_BODY()
	
protected:

	/** True if the transition can only be used with a jump. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Config")
	uint8 bRequiresJump : 1;

	/** Whether or not to automatically calculate the required transition direction. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Config")
	uint8 bAutoCalculateDirection: 1;

	/** Direction the player needs to go to use this transition. */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Transition Config", meta=(EditCondition="!bAutoCalculateDirection"))
	FVector LocalTransitionDirection;

	/** Target of the transition */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Config")
	class AAdvKitZone* TargetZone;

	/** True to automatically set the target movement modes to what the zone has */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Config")
	uint8 bSetMovementModesFromZone: 1;

	/** Target movement mode of the transition */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Config")
	EAdvKitMovementMode TargetMovementMode;
	
	/** Target custom movement mode of the transition */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Config")
	uint8 TargetCustomMovementMode;

	/** Which characters will be able to use this transition */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Config")
	TArray<TSubclassOf<AAdvKitCharacter>> UseableBy;

public:
	/**
	 * Constructor
	 */
	UAdvKitManualTransition();

	/**
	 * Creates the transition.
	 */
	virtual void CreateTransition();

#if WITH_EDITOR
	virtual bool CanEditChange(const UProperty* InProperty) const override;
#endif

};
