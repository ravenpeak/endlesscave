// Copyright 2015 Pascal Krabbe

#pragma once

#include "Object.h"
#include "Actions/AdvKitCharacterAction.h"
#include "Player/AdvKitCharacterMovementComponent.h"
#include "AdvKitCA_Router_ToFloor.generated.h"

/**
 * @brief Router action that redirects to other actions moving the character to the floor
 */
UCLASS(abstract)
class ADVKITRUNTIME_API UAdvKitCA_Router_ToFloor : public UAdvKitCharacterAction
{
	GENERATED_BODY()

protected:
	/** Action to take to climb from a wall to the platform above */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Wall")
	TSubclassOf<class UAdvKitCharacterAction> FromWallToPlatform;

	/** Action to take to climb from a wall to the platform below */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Wall")
	TSubclassOf<class UAdvKitCharacterAction> FromWallToGround;

public:
	/** Begin UAdvKitCharacterAction Interface */
	virtual FAdvKitActionResponse Start_Implementation(class UAdvKitCharacterAction_Arguments* Arguments = NULL, UAdvKitCharacterAction* InterruptedOther = NULL) override;
	virtual FAdvKitActionResponse Stop_Implementation(const UAdvKitCharacterAction* InterruptedBy = NULL) override;
	virtual TArray<TSubclassOf<UAdvKitCharacterAction>> GetAdditionalRequiredActions_Implementation() override;
	/** End UAdvKitCharacterAction Interface */
};
