// Copyright 2015 Pascal Krabbe

#pragma once

#include "Object.h"
#include "Actions/AdvKitCharacterAction.h"
#include "Player/AdvKitCharacterMovementComponent.h"
#include "AdvKitCA_Router_ToWall.generated.h"

/**
 * @brief Router action that redirects to other actions moving the character around a corner while
 * wall climbing.
 */
UCLASS(abstract, hidecategories = Action)
class ADVKITRUNTIME_API UAdvKitCA_Router_ToWall : public UAdvKitCharacterAction
{
	GENERATED_BODY()
protected:
	/** Action that takes care of moving the character around a concave corner to the left */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Wall")
	TSubclassOf<class UAdvKitCharacterAction> AroundInnerLeftCorner;

	/** Action that takes care of moving the character around a concave corner to the right */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Wall")
	TSubclassOf<class UAdvKitCharacterAction> AroundInnerRightCorner;

	/** Action that takes care of moving the character around a convex corner to the left */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Wall")
	TSubclassOf<class UAdvKitCharacterAction> AroundOuterLeftCorner;

	/** Action that takes care of moving the character around a convex corner to the right */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Wall")
	TSubclassOf<class UAdvKitCharacterAction> AroundOuterRightCorner;

public:
	/** Begin UAdvKitCharacterAction Interface */
	virtual FAdvKitActionResponse Start_Implementation(class UAdvKitCharacterAction_Arguments* Arguments = NULL, UAdvKitCharacterAction* InterruptedOther = NULL) override;
	virtual FAdvKitActionResponse Stop_Implementation(const UAdvKitCharacterAction* InterruptedBy = NULL) override;
	virtual TArray<TSubclassOf<UAdvKitCharacterAction>> GetAdditionalRequiredActions_Implementation() override;
	/** End UAdvKitCharacterAction Interface */
};
