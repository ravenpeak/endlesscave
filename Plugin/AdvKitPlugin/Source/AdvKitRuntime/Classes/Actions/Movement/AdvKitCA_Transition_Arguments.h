// Copyright 2015 Pascal Krabbe

#pragma once

#include "Actions/AdvKitCharacterAction_Arguments.h"
#include "Player/AdvKitCharacterMovementComponent.h"
#include "AdvKitCA_Transition_Arguments.generated.h"

/**
 * @brief Arguments for a UAdvKitCA_Transition action. @see UAdvKitCA_Transition
 */
UCLASS(BlueprintType, Blueprintable, hidecategories = Object)
class UAdvKitCA_Transition_Arguments : public UAdvKitCharacterAction_Arguments
{
	GENERATED_BODY()
public:
	/** Transition component along which the transition happens. */
	UPROPERTY(BlueprintReadWrite)
	UAdvKitTransitionComponent* Transition;

	/** Target zone of the transition */
	UPROPERTY(BlueprintReadWrite)
	class AAdvKitZone* Zone;
};
