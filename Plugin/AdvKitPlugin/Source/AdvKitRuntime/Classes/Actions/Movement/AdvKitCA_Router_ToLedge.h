// Copyright 2015 Pascal Krabbe

#pragma once

#include "Object.h"
#include "Actions/AdvKitCharacterAction.h"
#include "Player/AdvKitCharacterMovementComponent.h"
#include "AdvKitCA_Router_ToLedge.generated.h"

/**
 * @brief Router action that redirects to other actions moving the character to a ledge
 */
UCLASS(abstract, hidecategories = Action)
class ADVKITRUNTIME_API UAdvKitCA_Router_ToLedge : public UAdvKitCharacterAction
{
	GENERATED_BODY()

protected:
	/** Action to take when the character should jump from the ground to a ledge */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Walking")
	TSubclassOf<class UAdvKitCharacterAction> JumpUpToLedge;

	/** Action that handles what happens when the character walks over an edge to grab a ledge */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Walking")
	TSubclassOf<class UAdvKitCharacterAction> GrabLedgeAfterWalkingOver;

	/** This is used when the transition from ledge to ledge is instant because the ledges are right next to each other */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ledge")
	TSubclassOf<class UAdvKitCharacterAction> InstantTransition;

	/** Action that takes care of moving the character around a concave corner to the left */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ledge")
	TSubclassOf<class UAdvKitCharacterAction> AroundInnerLeftCorner;

	/** Action that takes care of moving the character around a concave corner to the right */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ledge")
	TSubclassOf<class UAdvKitCharacterAction> AroundInnerRightCorner;

	/** Action that takes care of moving the character around a convex corner to the left */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ledge")
	TSubclassOf<class UAdvKitCharacterAction> AroundOuterLeftCorner;

	/** Action that takes care of moving the character around a convex corner to the right */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ledge")
	TSubclassOf<class UAdvKitCharacterAction> AroundOuterRightCorner;

	/** This action handles when the character jumps vertically upwards to grab another ledge */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ledge")
	TSubclassOf<class UAdvKitCharacterAction> JumpUpFromLedgeToOtherLedge;

	/** This action handles when the character jumps vertically downwards to grab another ledge */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ledge")
	TSubclassOf<class UAdvKitCharacterAction> JumpDownFromLedgeToOtherLedge;

	/** This action handles when the character jumps horizontally to the left to grab another ledge */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ledge")
	TSubclassOf<class UAdvKitCharacterAction> JumpLeftFromLedgeToOtherLedge;

	/** This action handles when the character jumps horizontally to the right to grab another ledge */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ledge")
	TSubclassOf<class UAdvKitCharacterAction> JumpRightFromLedgeToOtherLedge;

	/** This action handles when the character jumps from a ladder horizontally to the left to grab a ledge */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ladder")
	TSubclassOf<class UAdvKitCharacterAction> JumpLeftFromLadderToLedge;

	/** This action handles when the character jumps from a ladder horizontally to the right to grab a ledge */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ladder")
	TSubclassOf<class UAdvKitCharacterAction> JumpRightFromLadderToLedge;

public:
	/** Begin UAdvKitCharacterAction Interface */
	virtual FAdvKitActionResponse Start_Implementation(class UAdvKitCharacterAction_Arguments* Arguments = NULL, UAdvKitCharacterAction* InterruptedOther = NULL) override;
	virtual FAdvKitActionResponse Stop_Implementation(const UAdvKitCharacterAction* InterruptedBy = NULL) override;
	virtual TArray<TSubclassOf<UAdvKitCharacterAction>> GetAdditionalRequiredActions_Implementation() override;
	/** End UAdvKitCharacterAction Interface */
};
