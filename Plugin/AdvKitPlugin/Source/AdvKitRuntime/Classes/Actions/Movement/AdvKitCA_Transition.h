// Copyright 2015 Pascal Krabbe

#pragma once

#include "Actions/AdvKitCharacterAction.h"
#include "Player/AdvKitCharacterMovementComponent.h"
#include "AdvKitCA_Transition.generated.h"

class UAdvKitCA_Transition_Arguments;

/**
 * @brief Action that makes the character change zones. It can play an animation that syncs with the
 * transition and dynamically interpolate between starting and end locations to make the transition
 * look smooth even if the animation does not line up correctly.
 */
UCLASS(abstract)
class ADVKITRUNTIME_API UAdvKitCA_Transition : public UAdvKitCharacterAction
{
	GENERATED_BODY()
protected:

	/** Arguments for the current transition */
	UPROPERTY()
	UAdvKitCA_Transition_Arguments* CurrentArguments;

	/** Flag to use interpolation in case animation does not line up with transition */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Transition Prediction")
	bool bUseTransitionPrediction;

	/** Use rotation part of transition prediction */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Transition Prediction", meta = (EditCondition = bUseTransitionPrediction))
	bool bUseTransitionPredictionRotation;

	/** Movement offset the character gets form the animation in character space */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Prediction", meta = (EditCondition = bUseTransitionPrediction))
	FVector LocalTranslationOffset;

	/** Use rotation part of transition prediction */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Transition Prediction", meta = (EditCondition = bUseTransitionPrediction))
	bool bUseTransitionPredictionLocation;

	/** Rotation offset the character gets form the animation in character space */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Prediction", meta = (EditCondition = bUseTransitionPrediction))
	FRotator LocalRotationOffset;

	/** Interpolation curve for the transition prediction */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition Prediction", meta = (EditCondition = bUseTransitionPrediction))
	FRuntimeFloatCurve TransitionCurve;

	/** Current time of the transition curve */
	float CurrentTransitionTime;

	/** Delta translation during the current transition */
	FVector TranslationDifference;
	
	/** Delta rotation during the current transition */
	FRotator RotationDifference;

	/** New location in target zone after the transition*/
	UPROPERTY()
	UAdvKitZoneLocation* NextLocationInZone;

	/** Set the target zone as the active one of the character at the start of the action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition")
	bool ApplyZoneOnStart;

	/** Set the target zone as the active one of the character at the end of the action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition")
	bool ApplyZoneOnStop;

	/** Animation Montage to play during the action. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation", meta = (Tooltip = "Animation Montage to play during the action."))
	UAnimMontage* AnimMontage;

public:
	/**
	 * Constructor
	 */
	UAdvKitCA_Transition(const FObjectInitializer& ObjectInitializer);

	//Begin UActorComponent Interface
	virtual void TickComponent(float DeltaSeconds, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
	//End UActorComponent Interface

	/**
	 * @return	The current arguments this action is using.
	 */
	const UAdvKitCA_Transition_Arguments* GetCurrentArguments() const;


protected:
	/** Begin UAdvKitCharacterAction Interface */
	virtual void OnMontageEnded_Implementation(UAnimMontage *EndedMontage, bool bInterrupted) override;
public:
	virtual bool CanBeInterruptedBy_Implementation(const UAdvKitCharacterAction* Other) const override;
	virtual FAdvKitActionResponse Start_Implementation(class UAdvKitCharacterAction_Arguments* Arguments = NULL, UAdvKitCharacterAction* InterruptedOther = NULL) override;
	virtual FAdvKitActionResponse Stop_Implementation(const UAdvKitCharacterAction* InterruptedBy = NULL) override;
	/** End UAdvKitCharacterAction Interface */

	/**
	 * Creates an arguments object for this action.
	 * @param	Zone 	Target zone for the transition
	 * @return	Arguments object.
	 */
	static class UAdvKitCharacterAction_Arguments* MakeArguments(AAdvKitZone* Zone);

	/**
	 * Creates an arguments object for this action.
	 * @param	Transition 	Transition to take during the action
	 * @return	Arguments object.
	 */
	static class UAdvKitCharacterAction_Arguments* MakeArguments(UAdvKitTransitionComponent* Transition);

};
