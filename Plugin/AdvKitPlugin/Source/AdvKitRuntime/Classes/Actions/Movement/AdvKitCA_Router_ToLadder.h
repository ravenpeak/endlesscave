// Copyright 2015 Pascal Krabbe

#pragma once

#include "Object.h"
#include "Actions/AdvKitCharacterAction.h"
#include "Player/AdvKitCharacterMovementComponent.h"
#include "AdvKitCA_Router_ToLadder.generated.h"

/**
 * @brief Router action that redirects to other actions moving the character to a ladder
 */
UCLASS(abstract, hidecategories = Action)
class ADVKITRUNTIME_API UAdvKitCA_Router_ToLadder : public UAdvKitCharacterAction
{
	GENERATED_BODY()
protected:
	/** Action to take when the character should start using the ladder from the bottom */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Walking")
	TSubclassOf<class UAdvKitCharacterAction> FromFloor;

	/** Action to take when the character should start using the ladder from the top */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Walking")
	TSubclassOf<class UAdvKitCharacterAction> FromAbove;

	/** Action to jump left from a ledge to a ladder */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ledge")
	TSubclassOf<class UAdvKitCharacterAction> JumpLeftFromLedge;

	/** Action to jump right from a ledge to a ladder */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ledge")
	TSubclassOf<class UAdvKitCharacterAction> JumpRightFromLedge;

	/** Action to jump up from a ledge to a ladder */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ledge")
	TSubclassOf<class UAdvKitCharacterAction> JumpUpFromLedge;

	/** Action to jump down from a ledge to a ladder */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ledge")
	TSubclassOf<class UAdvKitCharacterAction> JumpDownFromLedge;

	/** Action to jump left from a ladder to another ladder */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ladder")
	TSubclassOf<class UAdvKitCharacterAction> JumpLeftFromLadder;

	/** Action to jump right from a ladder to another ladder */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ladder")
	TSubclassOf<class UAdvKitCharacterAction> JumpRightFromLadder;

	/** Action to jump up from a ladder to another ladder */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ladder")
	TSubclassOf<class UAdvKitCharacterAction> JumpUpFromLadder;

	/** Action to jump down from a ladder to another ladder */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ladder")
	TSubclassOf<class UAdvKitCharacterAction> JumpDownFromLadder;

	/** Action to go down one step on the same ladder */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ladder")
	TSubclassOf<class UAdvKitCharacterAction> ClimbDownOneStep;

	/** Action to go up one step on the same ladder */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AdvKit|Ladder")
	TSubclassOf<class UAdvKitCharacterAction> ClimbUpOneStep;

public:
	/** Begin UAdvKitCharacterAction Interface */
	virtual FAdvKitActionResponse Start_Implementation(class UAdvKitCharacterAction_Arguments* Arguments = NULL, UAdvKitCharacterAction* InterruptedOther = NULL) override;
	virtual FAdvKitActionResponse Stop_Implementation(const UAdvKitCharacterAction* InterruptedBy = NULL) override;
	virtual TArray<TSubclassOf<UAdvKitCharacterAction>> GetAdditionalRequiredActions_Implementation() override;
	/** End UAdvKitCharacterAction Interface */
};
