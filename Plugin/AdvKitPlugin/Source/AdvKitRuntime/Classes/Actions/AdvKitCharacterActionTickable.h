// Copyright 2015 Pascal Krabbe

#pragma once

#include "Object.h"
#include "Player/AdvKitCharacterMovementComponent.h"
#include "Actions/AdvKitCharacterAction.h"
#include "AdvKitCharacterActionTickable.generated.h"

/**
 * @brief A character action that ticks while being active.
 */
UCLASS(abstract, Blueprintable)
class ADVKITRUNTIME_API UAdvKitCharacterActionTickable : public UAdvKitCharacterAction
{
	GENERATED_BODY()

public:
	/**
	 * Constructor
	 */
	UAdvKitCharacterActionTickable(const FObjectInitializer& ObjectInitializer);

	//Begin UAdvKitCharacterAction Interface
	virtual FAdvKitActionResponse Start_Implementation(class UAdvKitCharacterAction_Arguments* Arguments = NULL, UAdvKitCharacterAction* InterruptedOther = NULL) override;
	virtual FAdvKitActionResponse Stop_Implementation(const UAdvKitCharacterAction* InterruptedBy = NULL) override;
	//End UAdvKitCharacterAction Interface
};
