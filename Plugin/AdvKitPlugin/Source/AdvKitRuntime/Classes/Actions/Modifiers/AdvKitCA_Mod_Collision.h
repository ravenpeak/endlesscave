// Copyright 2015 Pascal Krabbe

#pragma once

#include "Object.h"
#include "Actions/AdvKitCharacterAction_CharacterModifier.h"
#include "AdvKitCA_Mod_Collision.generated.h"

/**
 * @brief Changes the characters collision in an action
 */
UCLASS(DisplayName="Modify Collision")
class ADVKITRUNTIME_API UAdvKitCA_Mod_Collision : public UAdvKitCharacterAction_CharacterModifier
{
	GENERATED_BODY()

public:

	/** Enable or disable collision. */
	UPROPERTY(EditAnywhere, Category = "Configuration", meta = (EditCondition = "bChangeCollision", Tooltip = "Enable or disable collision."))
	bool bEnableCollision;

	//Begin UAdvKitCharacterAction_CharacterModifier Interface
	virtual void ApplyToCharacter(class AAdvKitCharacter* ToCharacter, class UAdvKitCharacterMovementComponent* ToMovementComponent);
	//End UAdvKitCharacterAction_CharacterModifier Interface
};
