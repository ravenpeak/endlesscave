// Copyright 2015 Pascal Krabbe

#include "AdvKitRuntime.h"
#include "Actions/Modifiers/AdvKitCA_Mod_Collision.h"


void UAdvKitCA_Mod_Collision::ApplyToCharacter(class AAdvKitCharacter* ToCharacter, class UAdvKitCharacterMovementComponent* ToMovementComponent)
{
	if(!ToCharacter)
	{
		return;
	}
	
	ToCharacter->SetActorEnableCollision(bEnableCollision);
}
