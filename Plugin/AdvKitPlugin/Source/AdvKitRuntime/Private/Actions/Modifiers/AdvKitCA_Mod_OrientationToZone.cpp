// Copyright 2015 Pascal Krabbe

#include "AdvKitRuntime.h"
#include "Actions/Modifiers/AdvKitCA_Mod_OrientationToZone.h"
#include "Actions/Movement/AdvKitCA_Transition.h"
#include "Actions/Movement/AdvKitCA_Transition_Arguments.h"
#include "Environment/AdvKitZone.h"
#include "Environment/AdvKitTransitionComponent.h"

UAdvKitCA_Mod_OrientationToZone::UAdvKitCA_Mod_OrientationToZone(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bSetLocation = false;
	Location = FVector::ZeroVector;
	bSetRotation = true;
	Rotation = FRotator::ZeroRotator;
}

void UAdvKitCA_Mod_OrientationToZone::ApplyToCharacter(class AAdvKitCharacter* ToCharacter, class UAdvKitCharacterMovementComponent* ToMovementComponent)
{
	auto OwnerAction = Cast<UAdvKitCA_Transition>(GetOuter());
	if(!OwnerAction)
	{
		return;
	}

	auto Arguments = OwnerAction->GetCurrentArguments();
	if (!Arguments)
	{
		return;
	}

	auto Zone = Arguments->Zone;
	if (!Zone)
	{
		return;
	}

	if (bSetRotation)
	{
		ToCharacter->SetActorRotation(Zone->GetActorQuat()*Rotation.Quaternion());
	}

	if (bSetLocation)
	{
		ToCharacter->SetActorLocation(Zone->GetTransform().TransformPosition(Location));
	}
}
