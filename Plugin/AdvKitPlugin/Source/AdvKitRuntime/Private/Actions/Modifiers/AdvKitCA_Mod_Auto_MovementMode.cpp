// Copyright 2015 Pascal Krabbe

#include "AdvKitRuntime.h"
#include "Actions/Modifiers/AdvKitCA_Mod_Auto_MovementMode.h"
#include "Actions/Movement/AdvKitCA_Transition.h"
#include "Actions/Movement/AdvKitCA_Transition_Arguments.h"
#include "Environment/AdvKitZone.h"
#include "Environment/AdvKitTransitionComponent.h"


void UAdvKitCA_Mod_Auto_MovementMode::ApplyToCharacter(class AAdvKitCharacter* ToCharacter, class UAdvKitCharacterMovementComponent* ToMovementComponent)
{
	auto OwnerAction = Cast<UAdvKitCA_Transition>(GetOuter());
	if(!OwnerAction)
	{
		return;
	}

	auto Arguments = OwnerAction->GetCurrentArguments();
	if (!Arguments)
	{
		return;
	}

	auto Zone = Arguments->Zone;
	if (Zone)
	{
		ToMovementComponent->SetAdvMovementMode(Zone->GetPhysics(), Zone->GetCustomPhysics());
	}

	auto Transition = Arguments->Transition;
	if (Transition)
	{
		ToMovementComponent->SetAdvMovementMode(Transition->TargetPhysics, Transition->TargetCustomPhysics);
	}
}
