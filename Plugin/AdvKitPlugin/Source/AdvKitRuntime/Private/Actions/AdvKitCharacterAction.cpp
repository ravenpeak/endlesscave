// Copyright 2015 Pascal Krabbe

#include "AdvKitRuntime.h"
#include "Actions/AdvKitCharacterAction.h"
#include "Player/AdvKitCharacter.h"
#include "Player/AdvKitInventoryManager.h"

#include "Actions/AdvKitCharacterAction_StateConfiguration.h"
#include "Actions/AdvKitCharacterAction_CharacterModifier.h"

#include "Net/UnrealNetwork.h"

bool FAdvKitActionResponse::IsSuccess()
{
	return Type == EAdvKitActionResponseType::Success;
}

bool FAdvKitActionResponse::IsFailure()
{
	return Type == EAdvKitActionResponseType::Failure;
}

bool FAdvKitActionResponse::IsRedirect()
{
	return Type == EAdvKitActionResponseType::Redirect;
}

bool FAdvKitActionResponse::IsNotFound()
{
	return Type == EAdvKitActionResponseType::NotFound;
}

UAdvKitCharacterAction::UAdvKitCharacterAction(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bActive = false;
	bReplicates = true;
}

void UAdvKitCharacterAction::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UAdvKitCharacterAction, CharacterOwner);
	DOREPLIFETIME(UAdvKitCharacterAction, MovementComponent);
}

void UAdvKitCharacterAction::Initialize(AAdvKitCharacter *NewCharacterOwner)
{
	check(NewCharacterOwner);
	CharacterOwner = NewCharacterOwner;
	MovementComponent = Cast<UAdvKitCharacterMovementComponent>(CharacterOwner->GetMovementComponent());
}

void UAdvKitCharacterAction::PlayAnimMontage(UAnimMontage *Montage)
{
	if (!CharacterOwner)
		return;

	CharacterOwner->PlayAnimMontage(Montage);

	if (!CharacterOwner->Mesh)
		return;
	
	if (!CharacterOwner->Mesh->GetAnimInstance())
		return;

	OnMontageEndedDelegate.BindUObject(this, &UAdvKitCharacterAction::OnMontageEnded);
	CharacterOwner->Mesh->GetAnimInstance()->Montage_SetBlendingOutDelegate(OnMontageEndedDelegate, Montage);
	//CharacterOwner->Mesh->GetAnimInstance()->Montage_SetEndDelegate(OnMontageEndedDelegate, Montage);
}

void UAdvKitCharacterAction::OnMontageEnded_Implementation(UAnimMontage *EndedMontage, bool bInterrupted)
{
}

FAdvKitActionResponse UAdvKitCharacterAction::Start_Implementation(class UAdvKitCharacterAction_Arguments* Arguments, UAdvKitCharacterAction* InterruptedOther)
{
	bActive = true;
	ApplyStartModifiers();

	return Succeeded();
}

FAdvKitActionResponse UAdvKitCharacterAction::Stop_Implementation(const UAdvKitCharacterAction* InterruptedBy)
{
	ApplyStopModifiers();

	OnMontageEndedDelegate.Unbind();
	bActive = false;
	return Succeeded();
}

bool UAdvKitCharacterAction::CanBeInterruptedBy_Implementation(const UAdvKitCharacterAction* Other) const
{
	//Dummy implementation to be overridden in subclass
	return true;
}

bool UAdvKitCharacterAction::CanInterrupt_Implementation(const UAdvKitCharacterAction* Other) const
{
	//Dummy implementation to be overridden in subclass
	return false;
}

bool UAdvKitCharacterAction::HasEnded() const
{
	return !bActive;
}

FAdvKitActionResponse UAdvKitCharacterAction::Redirect(TSubclassOf<class UAdvKitCharacterAction> ToAction, UAdvKitCharacterAction_Arguments* WithArguments) 
{
	FAdvKitActionResponse NewResponse;
	NewResponse.Type = EAdvKitActionResponseType::Redirect;
	NewResponse.RedirectAction = ToAction;
	NewResponse.RedirectArguments = WithArguments;

	return NewResponse;
}

FAdvKitActionResponse UAdvKitCharacterAction::Succeeded() 
{
	FAdvKitActionResponse NewResponse;
	NewResponse.Type = EAdvKitActionResponseType::Success;

	return NewResponse;
}

FAdvKitActionResponse UAdvKitCharacterAction::Failed() 
{
	FAdvKitActionResponse NewResponse;
	NewResponse.Type = EAdvKitActionResponseType::Failure;

	return NewResponse;
}

TArray<TSubclassOf<UAdvKitCharacterAction>> UAdvKitCharacterAction::GetAdditionalRequiredActions_Implementation()
{
	//Dummy implementation to be overridden in subclass
	return TArray<TSubclassOf<UAdvKitCharacterAction>>();
}


void UAdvKitCharacterAction::AccumulateRootMotion(FVector MovementDelta, FRotator NewRotation)
{
	FTransform RootMotionTransform;

	if (!CharacterOwner || !MovementComponent)
	{
		return;
	}

	RootMotionTransform.SetRotation(
		NewRotation.Quaternion()
		*
		CharacterOwner->GetActorRotation().Quaternion().Inverse()
//		*
//		CharacterOwner->GetMesh()->RelativeRotation.Quaternion()
		);

	FVector LocalDelta = CharacterOwner->GetMesh()->GetComponentTransform().InverseTransformVector(MovementDelta);
	RootMotionTransform.SetLocation(LocalDelta);

	MovementComponent->RootMotionParams.Accumulate(RootMotionTransform);
}

void UAdvKitCharacterAction::AccumulateRootMotionRotation(FRotator NewRotation)
{
	FTransform RootMotionTransform;

	RootMotionTransform.SetRotation(
		NewRotation.Quaternion()
		*
		CharacterOwner->GetMesh()->RelativeRotation.Quaternion()
		);

	MovementComponent->RootMotionParams.Accumulate(RootMotionTransform);
}

void UAdvKitCharacterAction::AddRootMotionMovement(FVector GlobalDelta)
{
	FTransform RootMotionTransform;

	FVector LocalDelta = CharacterOwner->GetMesh()->GetComponentTransform().InverseTransformVector(GlobalDelta);
	RootMotionTransform.SetLocation(LocalDelta);

	MovementComponent->RootMotionParams.Accumulate(RootMotionTransform);
}

void UAdvKitCharacterAction::AddRootMotionRotation(FRotator AdditionalRotation)
{
	FTransform RootMotionTransform;
	RootMotionTransform.SetRotation(AdditionalRotation.Quaternion());
	MovementComponent->RootMotionParams.Accumulate(RootMotionTransform);
}

void UAdvKitCharacterAction::ApplyStartModifiers()
{
	//ConfigOnStart->ApplyToCharacter(CharacterOwner, MovementComponent);
	for (auto Config : StartModifiers)
	{
		if (!IsValid(Config))
		{
			continue;
		}

		Config->ApplyToCharacter(CharacterOwner, MovementComponent);
	}
}


void UAdvKitCharacterAction::ApplyStopModifiers()
{
	//ConfigOnStop->ApplyToCharacter(CharacterOwner, MovementComponent);
	for (auto Config : StopModifiers)
	{
		if (!IsValid(Config))
		{
			continue;
		}

		Config->ApplyToCharacter(CharacterOwner, MovementComponent);
	}
}

AAdvKitCharacter* UAdvKitCharacterAction::GetCharacterOwner()
{
	return CharacterOwner;
}

UAdvKitCharacterMovementComponent* UAdvKitCharacterAction::GetMovementComponent()
{
	return MovementComponent;
}
