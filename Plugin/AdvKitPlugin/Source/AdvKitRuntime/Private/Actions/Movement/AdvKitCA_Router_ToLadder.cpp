// Copyright 2015 Pascal Krabbe

#include "AdvKitRuntime.h"
#include "Actions/Movement/AdvKitCA_Transition_Arguments.h"
#include "Actions/Movement/AdvKitCA_Router_ToLadder.h"
#include "Environment/AdvKitZone.h"
#include "Player/AdvKitCharacter.h"
#include "Environment/AdvKitTransitionComponent.h"

FAdvKitActionResponse UAdvKitCA_Router_ToLadder::Start_Implementation(class UAdvKitCharacterAction_Arguments* Arguments, UAdvKitCharacterAction* InterruptedOther)
{
	//Cannot work without arguments
	UAdvKitCA_Transition_Arguments* TransitionArgs = Cast<UAdvKitCA_Transition_Arguments>(Arguments);
	if (!IsValid(TransitionArgs) || !TransitionArgs->Zone)
	{
		return Failed();
	}

	FTransform CurrentTransform = CharacterOwner->GetTransform();

	auto CurrentZone = CharacterOwner->GetZone();
	//Same Ladder
	if (CurrentZone && CurrentZone == TransitionArgs->Zone)
	{
		auto* Transition = TransitionArgs->Transition;
		if (!Transition)
		{
			return Failed();
		}

		FVector CharacterLocation = CharacterOwner->GetActorLocation();
			
		FVector LocalStart = CurrentTransform.InverseTransformPositionNoScale(Transition->GetClosestTargetPosition(CharacterLocation));
		FVector LocalEnd = CurrentTransform.InverseTransformPositionNoScale(Transition->GetClosestSourcePosition(CharacterLocation));

		float ZDifference = LocalStart.Z - LocalEnd.Z;

		if (ZDifference > 0)
		{
			return Redirect(ClimbUpOneStep, TransitionArgs);
		}
		else if (ZDifference < 0)
		{
			return Redirect(ClimbDownOneStep, TransitionArgs);
		}
	}

	FVector NewLadderPosLocal = CurrentTransform.InverseTransformPositionNoScale(TransitionArgs->Zone->GetActorLocation());

	//From ground to ladder
	if (MovementComponent->IsWalking())
	{
		//Ladder is above 
		if (NewLadderPosLocal.Z > 0.0f)
		{
			return Redirect(FromFloor, Arguments);
		}
		//Ladder is below
		else if (NewLadderPosLocal.Z < 0.0f)
		{
			return Redirect(FromAbove, Arguments);
		}
	}
	//From ledge to ladder
	else if (MovementComponent->IsClimbingLedge())
	{
		//Ladder is right from character
		if (NewLadderPosLocal.Y > 0)
		{
			return Redirect(JumpRightFromLedge, TransitionArgs);
		}

		//Ladder is left from character
		if (NewLadderPosLocal.Y < 0)
		{
			return Redirect(JumpLeftFromLedge, TransitionArgs);
		}

		//Ladder is above character
		if (NewLadderPosLocal.Z > CharacterOwner->CapsuleComponent->GetScaledCapsuleHalfHeight())
		{
			return Redirect(JumpUpFromLedge, TransitionArgs);
		}

		////Ladder is below character
		//if (NewLadderPosLocal.Z < CharacterOwner->LedgeCheckJumpdown->GetRelativeLocation().Z*Half)
		//{
		//	return Redirect(JumpDownFromLadder, TransitionArgs);
		//}
	}
	//From ladder to ladder
	else if (MovementComponent->IsClimbingLadder())
	{
		//Ladder is right from character
		if (NewLadderPosLocal.Y > 0)
		{
			return Redirect(JumpRightFromLadder, TransitionArgs);
		}

		//Ladder is left from character
		if (NewLadderPosLocal.Y < 0)
		{
			return Redirect(JumpLeftFromLadder, TransitionArgs);
		}

		//Ladder is above character
		if (NewLadderPosLocal.Z > CharacterOwner->CapsuleComponent->GetScaledCapsuleHalfHeight())
		{
			return Redirect(JumpUpFromLadder, TransitionArgs);
		}

		//Ladder is below character
		if (NewLadderPosLocal.Z < CharacterOwner->CapsuleComponent->GetScaledCapsuleHalfHeight())
		{
			return Redirect(JumpDownFromLadder, TransitionArgs);
		}
	}


	return Failed();
}

FAdvKitActionResponse UAdvKitCA_Router_ToLadder::Stop_Implementation(const UAdvKitCharacterAction* InterruptedBy)
{
	return Succeeded();
}


TArray<TSubclassOf<UAdvKitCharacterAction>> UAdvKitCA_Router_ToLadder::GetAdditionalRequiredActions_Implementation()
{
	//Add redirect actions
	TArray<TSubclassOf<UAdvKitCharacterAction>> AdditionalActions;

	AdditionalActions.AddUnique(FromFloor);
	AdditionalActions.AddUnique(FromAbove);
	AdditionalActions.AddUnique(JumpLeftFromLedge);
	AdditionalActions.AddUnique(JumpRightFromLedge);
	AdditionalActions.AddUnique(JumpUpFromLedge);
	AdditionalActions.AddUnique(JumpDownFromLedge);
	AdditionalActions.AddUnique(JumpLeftFromLadder);
	AdditionalActions.AddUnique(JumpRightFromLadder);
	AdditionalActions.AddUnique(JumpUpFromLadder);
	AdditionalActions.AddUnique(JumpDownFromLadder);
	AdditionalActions.AddUnique(ClimbUpOneStep);
	AdditionalActions.AddUnique(ClimbDownOneStep);

	return AdditionalActions;
}