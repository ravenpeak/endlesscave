// Copyright 2015 Pascal Krabbe

#include "AdvKitRuntime.h"
#include "Actions/Movement/AdvKitCA_Transition_Arguments.h"
#include "Actions/Movement/AdvKitCA_Router_ToWall.h"
#include "Environment/AdvKitZone.h"
#include "Player/AdvKitCharacter.h"

 
FAdvKitActionResponse UAdvKitCA_Router_ToWall::Start_Implementation(class UAdvKitCharacterAction_Arguments* Arguments, UAdvKitCharacterAction* InterruptedOther)
{
	//Cannot work without arguments
	UAdvKitCA_Transition_Arguments* TransitionArgs = Cast<UAdvKitCA_Transition_Arguments>(Arguments);
	if (!IsValid(TransitionArgs))
	{
		return Failed();
	}

	//New zone needs to be a wall
	auto NewWall = TransitionArgs->Zone;
	if (!IsValid(NewWall) || ! NewWall->HasPhysics(EAdvKitMovementMode::ClimbingWall))
	{
		return Failed();
	}

	//This action can only handle wall corners so character needs to be on a wall already
	if (!MovementComponent->IsClimbingWall())
	{
		return Failed();
	}

	//Query where the character would be on the new wall
	FVector HalfExtent = MovementComponent->GetHalfExtentForZone(NewWall);
	FVector WorldNewWallLocation = NewWall->ConstrainPositionToZone(CharacterOwner->GetActorLocation(), HalfExtent, CharacterOwner->GetActorRotation());
	FVector LocalNewWallPosition = CharacterOwner->GetTransform().InverseTransformPositionNoScale(WorldNewWallLocation);

	//New wall in front of character
	if (LocalNewWallPosition.X > HalfExtent.X)
	{
		//New wall is right from character
		if (LocalNewWallPosition.Y > 0)
		{
			return Redirect(AroundOuterRightCorner, TransitionArgs);
		}
		//New wall is left from character
		else
		{
			return Redirect(AroundOuterLeftCorner, TransitionArgs);
		}
	}
	//New wall behind character
	else
	{
		//New wall is right from character
		if (LocalNewWallPosition.Y > 0)
		{
			return Redirect(AroundInnerRightCorner, TransitionArgs);
		}
		//New wall is left from character
		else
		{
			return Redirect(AroundInnerLeftCorner, TransitionArgs);
		}
	}

	//Nothing applies
	return Failed();
}

FAdvKitActionResponse UAdvKitCA_Router_ToWall::Stop_Implementation(const UAdvKitCharacterAction* InterruptedBy)
{
	return Succeeded();
}

TArray<TSubclassOf<UAdvKitCharacterAction>> UAdvKitCA_Router_ToWall::GetAdditionalRequiredActions_Implementation()
{
	//Add all redirect actions
	TArray<TSubclassOf<UAdvKitCharacterAction>> AdditionalActions;

	AdditionalActions.AddUnique(AroundInnerLeftCorner);
	AdditionalActions.AddUnique(AroundInnerRightCorner);
	AdditionalActions.AddUnique(AroundOuterLeftCorner);
	AdditionalActions.AddUnique(AroundOuterRightCorner);

	return AdditionalActions;
}