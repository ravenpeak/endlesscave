// Copyright 2015 Pascal Krabbe

#include "AdvKitRuntime.h"
#include "Actions/Movement/AdvKitCA_Transition.h"
#include "Actions/Movement/AdvKitCA_Transition_Arguments.h"
#include "Actions/Modifiers/AdvKitCA_Mod_MovementMode.h"

#include "Environment/AdvKitZoneLocation.h"
#include "Environment/AdvKitZone.h"
#include "Environment/AdvKitTransitionComponent.h"

#include "Player/AdvKitCharacter.h"

UAdvKitCA_Transition::UAdvKitCA_Transition(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bUseTransitionPrediction = false;
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;
	bAutoActivate = false;

	bUseTransitionPredictionRotation = true;
	bUseTransitionPredictionLocation = true;

	auto MovementModeMod = ObjectInitializer.CreateDefaultSubobject<UAdvKitCA_Mod_MovementMode>(this, "DefaultMovementMod");
	StartModifiers.Add(MovementModeMod);

	ApplyZoneOnStop = true;

	//Add default keys to transition curve forming a straight line
	FKeyHandle Handle1;
	TransitionCurve.GetRichCurve()->AddKey(0, 0, false, Handle1);
	TransitionCurve.GetRichCurve()->GetKey(Handle1).TangentMode = ERichCurveTangentMode::RCTM_Auto;
	TransitionCurve.GetRichCurve()->GetKey(Handle1).InterpMode = ERichCurveInterpMode::RCIM_Cubic;

	FKeyHandle Handle2;
	TransitionCurve.GetRichCurve()->AddKey(1, 1, false, Handle2);
	TransitionCurve.GetRichCurve()->GetKey(Handle2).TangentMode = ERichCurveTangentMode::RCTM_Auto;
	TransitionCurve.GetRichCurve()->GetKey(Handle2).InterpMode = ERichCurveInterpMode::RCIM_Cubic;

	TransitionCurve.GetRichCurve()->AutoSetTangents();
}

void UAdvKitCA_Transition::OnMontageEnded_Implementation(UAnimMontage *EndedMontage, bool bInterrupted)
{
	if (EndedMontage != AnimMontage)
	{
		return;
	}

	if (HasEnded())
	{
		//Already ended
		return;
	}

	Stop();
}

bool UAdvKitCA_Transition::CanBeInterruptedBy_Implementation(const UAdvKitCharacterAction* Other) const
{
	return false;
}


FAdvKitActionResponse UAdvKitCA_Transition::Start_Implementation(class UAdvKitCharacterAction_Arguments* Arguments, UAdvKitCharacterAction* InterruptedOther)
{
	//Cannot do anything without arguments
	CurrentArguments = Cast<UAdvKitCA_Transition_Arguments>(Arguments);
	if (!CurrentArguments)
	{
		return Failed();
	}

	//Set the new zone if required
	if (ApplyZoneOnStart)
	{
		MovementComponent->SetZone(CurrentArguments ? CurrentArguments->Zone : nullptr);
	}

	FAdvKitActionResponse Response = Super::Start_Implementation(Arguments, InterruptedOther);

	if (IsValid(AnimMontage))
	{
		PlayAnimMontage(AnimMontage);
	}

	//Without transition prediction the work is done here
	CurrentTransitionTime = 0;
	if (!bUseTransitionPrediction)
	{
		if (IsValid(AnimMontage))
		{
			return Response;
		}

		return Stop();
	}

	TranslationDifference = FVector::ZeroVector;
	RotationDifference = FRotator::ZeroRotator;

	FRotator RotationAfterMontage = CharacterOwner->GetActorRotation() + LocalRotationOffset;
	FVector LocationAfterMontage = CharacterOwner->GetTransform().TransformPositionNoScale(LocalTranslationOffset);

	//Calculate the necessary delta for the transition
	if (CurrentArguments->Zone)
	{

		FVector HalfExtent = MovementComponent->GetHalfExtentForZone(CurrentArguments->Zone);
		NextLocationInZone = CurrentArguments->Zone->GetClosestZoneLocation(
			LocationAfterMontage,
			HalfExtent, 
			RotationAfterMontage);

		EAdvKitMovementMode TargetPhysics = CurrentArguments->Zone->GetPhysics();
		uint8 TargetCustomPhysics = CurrentArguments->Zone->GetCustomPhysics();

		//Find out where exactly the character will be positioned after the transition
		FVector OffsetFromZone = MovementComponent->GetDesiredOffsetFromZone(CurrentArguments->Zone, NextLocationInZone);
		TranslationDifference = NextLocationInZone->GetGlobalPosition() + OffsetFromZone - LocationAfterMontage;
		
		//DrawDebugLine(GetWorld(), CharacterOwner->GetActorLocation(), NextLocationInZone->GetGlobalPosition() + OffsetFromZone, FColor::Yellow, false, 30.0f);
		//DrawDebugDirectionalArrow(GetWorld(), LocationAfterMontage, LocationAfterMontage + TranslationDifference, 10, FColor::Green, false, 30.0f);

		//Calculate delta rotation
		RotationDifference = MovementComponent->GetDesiredRotationInZone(CurrentArguments->Zone, NextLocationInZone) - RotationAfterMontage;
		
		//Transform to shortest path
		RotationDifference = RotationDifference.Quaternion().Rotator();

		SetActive(true);
		return Response;
	}

	//Only transition was given, but no zone, so it must be an exit transition
	if (CurrentArguments->Transition)
	{
		SetActive(true);
		FVector TargetLocation = CurrentArguments->Transition->GetClosestTargetPosition(CharacterOwner->GetActorLocation());
		TranslationDifference = TargetLocation - LocationAfterMontage;

		return Response;
	}

	return Response;
}

FAdvKitActionResponse UAdvKitCA_Transition::Stop_Implementation(const UAdvKitCharacterAction* InterruptedBy)
{
	SetActive(false);
	auto Result = Super::Stop_Implementation(InterruptedBy);

	if (!ApplyZoneOnStop)
	{
		return Result;
	}
	
	//If there has been transition prediction make sure the character is exactly where it was predicted to be
	if (bUseTransitionPrediction)
	{
		MovementComponent->SetZoneLocation(NextLocationInZone);
		//Clear property for GC
		NextLocationInZone = nullptr;
	}
	//No prediction, just set the new zone
	else
	{
		MovementComponent->SetZone(CurrentArguments ? CurrentArguments->Zone : nullptr);
	}

	return Result;
}

void UAdvKitCA_Transition::TickComponent(float DeltaSeconds, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaSeconds, TickType, ThisTickFunction);

	//If there is no transition interpolation, nothing needs to be done
	if (!bUseTransitionPrediction)
	{
		return;
	}

	//Get previous value
	float LastAlphaValue = TransitionCurve.GetRichCurve()->Eval(CurrentTransitionTime);

	//Add alpha time to transition time
	CurrentTransitionTime += DeltaSeconds;

	//Get new value
	float NewAlphaValue = TransitionCurve.GetRichCurve()->Eval(CurrentTransitionTime);

	//Calculate how much interpolation needs to be done this frame
	float DifAlpha = NewAlphaValue - LastAlphaValue;

	//Apply orientation delta
	if (bUseTransitionPredictionLocation)
	{
		AddRootMotionMovement(TranslationDifference*DifAlpha);
	}

	if (bUseTransitionPredictionRotation && !CharacterOwner->bLockTarget)
	{
		AddRootMotionRotation(RotationDifference * DifAlpha);
	}

	//If the animation governs the transitions there is no need to make the interpolation curve end the action
	if (AnimMontage != nullptr)
	{
		return;
	}

	//No animation means the transition curve has full controll over ending the action.
	if (CurrentTransitionTime > TransitionCurve.GetRichCurve()->GetLastKey().Time)
	{
		Stop();
	}
}

const UAdvKitCA_Transition_Arguments * UAdvKitCA_Transition::GetCurrentArguments() const
{
	return CurrentArguments;
}


class UAdvKitCharacterAction_Arguments* UAdvKitCA_Transition::MakeArguments(AAdvKitZone* Zone)
{
	UAdvKitCA_Transition_Arguments* Arguments = NewObject<UAdvKitCA_Transition_Arguments>();
	Arguments->Zone = Zone;
	return Arguments;
}

UAdvKitCharacterAction_Arguments* UAdvKitCA_Transition::MakeArguments(UAdvKitTransitionComponent* Transition)
{
	UAdvKitCA_Transition_Arguments* Arguments = NewObject<UAdvKitCA_Transition_Arguments>();
	Arguments->Transition = Transition;
	if (Transition)
	{
		Arguments->Zone = Transition->TargetZone.Get();
	}
	return Arguments;
}
