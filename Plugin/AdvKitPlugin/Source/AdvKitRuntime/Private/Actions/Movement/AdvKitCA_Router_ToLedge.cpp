// Copyright 2015 Pascal Krabbe

#include "AdvKitRuntime.h"
#include "Actions/Movement/AdvKitCA_Transition_Arguments.h"
#include "Actions/Movement/AdvKitCA_Router_ToLedge.h"
#include "Player/AdvKitCharacter.h"
#include "Environment/AdvKitZone.h"

FAdvKitActionResponse UAdvKitCA_Router_ToLedge::Start_Implementation(class UAdvKitCharacterAction_Arguments* Arguments, UAdvKitCharacterAction* InterruptedOther)
{
	//Cannot work without arguments
	UAdvKitCA_Transition_Arguments* TransitionArgs = Cast<UAdvKitCA_Transition_Arguments>(Arguments);
	if (!IsValid(TransitionArgs))
	{
		return Failed();
	}

	AAdvKitZone* CurrentZone = MovementComponent->GetZone();

	//New zone needs to be a ledge
	AAdvKitZone* NewZone = TransitionArgs->Zone;
	if (!NewZone || !NewZone->HasPhysics(EAdvKitMovementMode::ClimbingLedge))
	{
		return Failed();
	}

	//Query where the character would be on the new ledge
	FTransform CharacterTransform = CharacterOwner->GetTransform();
	FRotator CharacterRotation = CharacterOwner->GetActorRotation();
	FVector CharacterLocation = CharacterOwner->GetActorLocation();

	FVector CurrentLedgeLocation = CharacterOwner->GetActorLocation();
	if (CurrentZone)
	{
		FVector HalfExtent = MovementComponent->GetHalfExtentForZone(CurrentZone);
		CurrentLedgeLocation = CurrentZone->ConstrainPositionToZone(CharacterLocation, HalfExtent, CharacterRotation);
	}
	FVector LedgePosLocal = CharacterTransform.InverseTransformPositionNoScale(CurrentLedgeLocation);

	FVector NewHalfExtent = MovementComponent->GetHalfExtentForZone(NewZone);
	FVector NewLocation = NewZone->ConstrainPositionToZone(CharacterLocation, NewHalfExtent, CharacterRotation);
	FVector NewZonePosLocal = CharacterTransform.InverseTransformPositionNoScale(NewLocation);

	//Transition from ledge to ledge
	if (MovementComponent->IsClimbingLedge())
	{
		//Ledges meet
		if (FMath::IsNearlyZero(FVector::Dist(NewZonePosLocal, LedgePosLocal)))
		{
			return Redirect(InstantTransition, Arguments);
		}

		//New ledge is above this one
		if (NewZonePosLocal.Z - LedgePosLocal.Z > 10.0f)
		{
			return Redirect(JumpUpFromLedgeToOtherLedge, Arguments);
		}

		//New ledge is below this one
		if (NewZonePosLocal.Z - LedgePosLocal.Z < -10.0f)
		{
			return Redirect(JumpDownFromLedgeToOtherLedge, Arguments);
		}

		FVector NewZoneNormalLocal = CharacterTransform.InverseTransformVectorNoScale(NewZone->GetActorForwardVector());

		const float NormalMinDot = 0.5f;

		//Ledge is right from character
		if (NewZonePosLocal.Y > 0)
		{
			float RightDot = FVector::DotProduct(FVector(0, 1, 0), NewZoneNormalLocal);

			//Ledge is facing outwards from character
			if (RightDot > NormalMinDot)
			{
				return Redirect(AroundOuterRightCorner, TransitionArgs);
			}

			//Ledge is facing inwards to character
			if (RightDot < -NormalMinDot)
			{
				return Redirect(AroundInnerRightCorner, TransitionArgs);
			}

			//Ledge is facing the same way as the current ledge
			float ForwardDot = FVector::DotProduct(FVector::ForwardVector, NewZoneNormalLocal);
			if (ForwardDot < -NormalMinDot)
			{
				return Redirect(JumpRightFromLedgeToOtherLedge, TransitionArgs);
			}
		}

		//Ledge is left from character
		if (NewZonePosLocal.Y < 0)
		{
			float RightDot = FVector::DotProduct(FVector(0, -1, 0), NewZoneNormalLocal);

			//Ledge is facing outwards from character
			if (RightDot > NormalMinDot)
			{
				return Redirect(AroundOuterLeftCorner, TransitionArgs);
			}

			//Ledge is facing inwards to character
			if (RightDot < -NormalMinDot)
			{
				return Redirect(AroundInnerLeftCorner, TransitionArgs);
			}

			//Ledge is facing the same way as the current ledge
			float ForwardDot = FVector::DotProduct(FVector::ForwardVector, NewZoneNormalLocal);
			if (ForwardDot < -NormalMinDot)
			{
				return Redirect(JumpLeftFromLedgeToOtherLedge, TransitionArgs);
			}
		}
	}	
	//Transition from ladder to ledge
	else if (MovementComponent->IsClimbingLadder())
	{
		//Ledge is right from character
		if (NewZonePosLocal.Y > 0)
		{
			return Redirect(JumpRightFromLadderToLedge, TransitionArgs);
		}

		//Ledge is left from character
		if (NewZonePosLocal.Y < 0)
		{
			return Redirect(JumpLeftFromLadderToLedge, TransitionArgs);
		}
	}
	//Transition from walking to ledge
	else if (MovementComponent->IsWalking())
	{
		//Ledge is above character
		if (NewZonePosLocal.Z > 0)
		{
			return Redirect(JumpUpToLedge, TransitionArgs);
		}

		//Ledge is below character
		if (NewZonePosLocal.Z < 0)
		{
			return Redirect(GrabLedgeAfterWalkingOver, TransitionArgs);
		}
	}


	return Failed();
}

FAdvKitActionResponse UAdvKitCA_Router_ToLedge::Stop_Implementation(const UAdvKitCharacterAction* InterruptedBy)
{
	return Succeeded();
}


TArray<TSubclassOf<UAdvKitCharacterAction>> UAdvKitCA_Router_ToLedge::GetAdditionalRequiredActions_Implementation()
{
	//Add all redirect actions
	TArray<TSubclassOf<UAdvKitCharacterAction>> AdditionalActions;

	AdditionalActions.AddUnique(InstantTransition);
	AdditionalActions.AddUnique(JumpUpToLedge);
	AdditionalActions.AddUnique(GrabLedgeAfterWalkingOver);
	AdditionalActions.AddUnique(AroundInnerLeftCorner);
	AdditionalActions.AddUnique(AroundInnerRightCorner);
	AdditionalActions.AddUnique(AroundOuterLeftCorner);
	AdditionalActions.AddUnique(AroundOuterRightCorner);
	AdditionalActions.AddUnique(JumpUpFromLedgeToOtherLedge);
	AdditionalActions.AddUnique(JumpDownFromLedgeToOtherLedge);
	AdditionalActions.AddUnique(JumpLeftFromLedgeToOtherLedge);
	AdditionalActions.AddUnique(JumpRightFromLedgeToOtherLedge);
	AdditionalActions.AddUnique(JumpLeftFromLadderToLedge);
	AdditionalActions.AddUnique(JumpRightFromLadderToLedge);

	return AdditionalActions;
}
