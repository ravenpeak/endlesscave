// Copyright 2015 Pascal Krabbe

#include "AdvKitRuntime.h"
#include "Actions/Movement/AdvKitCA_Transition_Arguments.h"
#include "Actions/Movement/AdvKitCA_Router_ToFloor.h"
#include "Environment/AdvKitZone.h"
#include "Player/AdvKitCharacter.h"

FAdvKitActionResponse UAdvKitCA_Router_ToFloor::Start_Implementation(class UAdvKitCharacterAction_Arguments* Arguments, UAdvKitCharacterAction* InterruptedOther)
{
	//Need transition arguments for redirects
	UAdvKitCA_Transition_Arguments* TransitionArgs = Cast<UAdvKitCA_Transition_Arguments>(Arguments);
	if (!IsValid(TransitionArgs))
	{
		return Failed();
	}

	//Only works when climbing a wall
	if (!MovementComponent->IsClimbingWall())
	{
		return Failed();
	}

	FVector LocalVelocity = CharacterOwner->GetTransform().InverseTransformVectorNoScale(MovementComponent->Velocity);

	//character going up 
	if (LocalVelocity.Z > 0)
	{
		return Redirect(FromWallToPlatform, TransitionArgs);
	}

	//character going down
	if (LocalVelocity.Z < 0)
	{
		return Redirect(FromWallToGround, TransitionArgs);
	}

	return Failed();
}

FAdvKitActionResponse UAdvKitCA_Router_ToFloor::Stop_Implementation(const UAdvKitCharacterAction* InterruptedBy)
{
	return Succeeded();
}

TArray<TSubclassOf<UAdvKitCharacterAction>> UAdvKitCA_Router_ToFloor::GetAdditionalRequiredActions_Implementation()
{
	//Add redirect actions
	TArray<TSubclassOf<UAdvKitCharacterAction>> AdditionalActions;

	AdditionalActions.AddUnique(FromWallToPlatform);
	AdditionalActions.AddUnique(FromWallToGround);

	return AdditionalActions;
}