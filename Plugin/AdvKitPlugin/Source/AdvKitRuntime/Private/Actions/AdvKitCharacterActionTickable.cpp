// Copyright 2015 Pascal Krabbe

#include "AdvKitRuntime.h"
#include "Actions/AdvKitCharacterActionTickable.h"

UAdvKitCharacterActionTickable::UAdvKitCharacterActionTickable(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;
	bAutoActivate = false;
}

FAdvKitActionResponse UAdvKitCharacterActionTickable::Start_Implementation(class UAdvKitCharacterAction_Arguments* Arguments, UAdvKitCharacterAction* InterruptedOther)
{
	SetActive(true);

	return Super::Start_Implementation(Arguments, InterruptedOther);
}

FAdvKitActionResponse UAdvKitCharacterActionTickable::Stop_Implementation(const UAdvKitCharacterAction* InterruptedBy)
{
	SetActive(false);
	
	return Super::Stop_Implementation(InterruptedBy);
}

