// Copyright 2015 Pascal Krabbe

#include "AdvKitRuntime.h"

#include "Environment/AdvKitManualTransition.h"


UAdvKitManualTransition::UAdvKitManualTransition()
{
	bAutoCalculateDirection = true;
	bSetMovementModesFromZone = true;
}

void UAdvKitManualTransition::CreateTransition()
{
	//Dummy implementation, override in subclass
}


#if WITH_EDITOR
bool UAdvKitManualTransition::CanEditChange(const UProperty* Property) const
{
	bool bIsEditable = Super::CanEditChange(Property);
	if (bIsEditable && Property != NULL)
	{
		if (Property->GetFName() == TEXT("TargetMovementMode")
			 || Property->GetFName() == TEXT("TargetCustomMovementMode"))
		{
			return !bSetMovementModesFromZone || !IsValid(TargetZone);
		}
	}

	return bIsEditable;
}
#endif