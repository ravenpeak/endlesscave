#include "AdvKitRuntime.h"
#include "Environment/Transitions/AdvKitTransitionComponentArea.h"
#include "Environment/AdvKitZone.h"

#if WITH_EDITORONLY_DATA

float UAdvKitTransitionComponentArea::ArrowDistanceUnits = 100.0f;

void UAdvKitTransitionComponentArea::ClearArrows()
{
	for (auto& Arrow : Arrows)
	{
		if (Arrow.IsValid())
		{
			Arrow->DestroyComponent();
		}
	}
	Arrows.Empty();
}

void UAdvKitTransitionComponentArea::AddArrow(const FVector& WorldStart, const FVector& WorldTarget, AActor* OptionalBase)
{
	FLinearColor AreaColor = UAdvKitTransitionComponent::StaticTransitionColor;// FColor(200,255,200);
	if (bIsDynamic)
	{
		AreaColor = UAdvKitTransitionComponent::DynamicTransitionColor;
	}

	TWeakObjectPtr<class UArrowComponent> TempArrow = CreateArrow(WorldStart, WorldTarget, OptionalBase);
	if (TempArrow.IsValid())
	{
		TempArrow->SetArrowColor_New(AreaColor);
		Arrows.Add(TempArrow);
	}
}
#endif

void UAdvKitTransitionComponentArea::Init(TSubclassOf<AAdvKitCharacter> ForCharacter, const FVector& MinWorldSource, const FVector& MinWorldTarget, const FVector& MaxWorldSource, const FVector& MaxWorldTarget, EAdvKitMovementMode NewTargetPhysics, class AAdvKitZone* TargetZone, uint8 NewTargetCustomPhysics)
{
	AActor* BaseActor = Cast<AActor>(this->GetOuter());

	MinSourcePosition = FBasedPosition(BaseActor, MinWorldSource);
	MaxSourcePosition = FBasedPosition(BaseActor, MaxWorldSource);

	MinTargetPosition = FBasedPosition(TargetZone ? TargetZone : BaseActor, MinWorldTarget);
	MaxTargetPosition = FBasedPosition(TargetZone ? TargetZone : BaseActor, MaxWorldTarget);

	DynamicTransitionLength = FVector::Dist(MinWorldSource, MinWorldTarget);

	FVector MediumSource = (MinWorldSource + MaxWorldSource)*0.5f;
	FVector MediumTarget = (MinWorldTarget + MaxWorldTarget)*0.5f;
	TransitionDirection = BaseActor->GetTransform().InverseTransformVector(MediumTarget - MediumSource).GetSafeNormal();

	UseableBy = ForCharacter;

	this->TargetZone = TargetZone;
	TargetPhysics = NewTargetPhysics;
	TargetCustomPhysics = NewTargetCustomPhysics;

	auto BaseRoot = BaseActor->GetRootComponent();
	if (BaseRoot && BaseRoot->Mobility == EComponentMobility::Movable)
	{
		bIsDynamic = true;
	}

	if (TargetZone)
	{
		auto TargetRoot = TargetZone->GetRootComponent();
		if (TargetRoot && TargetRoot->Mobility == EComponentMobility::Movable)
		{
			bIsDynamic = true;
		}
	}


#if WITH_EDITORONLY_DATA
	ClearArrows();

	float Distance = FVector::Dist(MinWorldSource, MaxWorldSource);
	float ArrowCount = FPlatformMath::CeilToInt(Distance/ArrowDistanceUnits);
	float Step = 1.0f/ArrowCount;

	//float Arrows = Distance*ArrowsPerUnits;
	//float LerpAlpha = Distance / Arrows;

	for (float Alpha = 0; Alpha <= 1; Alpha += Step)
	{
		FVector LerpSource = FMath::Lerp<FVector>(MinWorldSource, MaxWorldSource, Alpha);
		FVector LerpTarget = FMath::Lerp<FVector>(MinWorldTarget, MaxWorldTarget, Alpha);
		AddArrow(LerpSource, LerpTarget, TargetZone);
	}
	AddArrow(MaxWorldSource, MaxWorldTarget, TargetZone);

	//AddArrow(MinWorldSource, MinWorldTarget, TargetZone);
	//AddArrow(MaxWorldSource, MaxWorldTarget, TargetZone);
	//AddArrow(MinWorldSource, MaxWorldTarget, TargetZone);
	//AddArrow(MaxWorldSource, MinWorldTarget, TargetZone);
#endif
}

bool UAdvKitTransitionComponentArea::IsDynamicTransitionValid()
{
	float CurrentDistance = FVector::Dist(*MinSourcePosition, *MinTargetPosition);
	return FMath::IsNearlyZero(CurrentDistance - DynamicTransitionLength, 1);
}

FVector UAdvKitTransitionComponentArea::GetClosestSourcePosition(const FVector& ToWorldPosition)
{
	return FMath::ClosestPointOnLine(*MinSourcePosition, *MaxSourcePosition, ToWorldPosition);
}

FVector UAdvKitTransitionComponentArea::GetClosestTargetPosition(const FVector& ToWorldPosition)
{
	return FMath::ClosestPointOnLine(*MinTargetPosition, *MaxTargetPosition, ToWorldPosition);
}

void UAdvKitTransitionComponentArea::OnComponentDestroyed()
{
	Super::OnComponentDestroyed();

	//Clean up all pointers
	MinSourcePosition = FBasedPosition();
	MaxSourcePosition = FBasedPosition();
	MinTargetPosition = FBasedPosition();
	MaxTargetPosition = FBasedPosition();

#if WITH_EDITORONLY_DATA
	ClearArrows();
#endif
}
