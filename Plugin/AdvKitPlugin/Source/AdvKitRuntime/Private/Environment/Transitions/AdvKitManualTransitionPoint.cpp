// Copyright 2015 Pascal Krabbe

#include "AdvKitRuntime.h"

#include "Environment/Transitions/AdvKitManualTransitionPoint.h"
#include "Environment/Transitions/AdvKitTransitionComponentPoint.h"
#include "Environment/AdvKitZone.h"

void UAdvKitManualTransitionPoint::CreateTransition()
{
	auto* SourceZone = Cast<AAdvKitZone>(GetOuter());
	if (!SourceZone)
	{
		return;
	}

	//Transform local positions to world space
	FVector WorldSource = SourceZone->GetTransform().TransformPositionNoScale(LocalSource);
	if (bSnapSource)
	{
		WorldSource = SourceZone->ConstrainPositionToZone(WorldSource, FVector::ZeroVector);
	}

	//Might not have a target zone, so use source instead
	FVector WorldTarget = SourceZone->GetTransform().TransformPositionNoScale(LocalTarget);
	if (bSnapTarget)
	{
		WorldTarget = SourceZone->ConstrainPositionToZone(WorldTarget, FVector::ZeroVector);
	}

	//Transform target location into world space if target zone is set
	if (TargetZone)
	{
		WorldTarget = TargetZone->GetTransform().TransformPositionNoScale(LocalTarget);
		if (bSnapTarget)
		{
			WorldTarget = TargetZone->ConstrainPositionToZone(WorldTarget, FVector::ZeroVector);
		}
	}

	//Create transitions for each configured character
	for (auto CharacterClass : UseableBy)
	{
		if (!IsValid(CharacterClass))
		{
			continue;
		}

		//Set transition movement modes
		auto MovementMode = TargetMovementMode;
		auto CustomMovementMode = TargetCustomMovementMode;

		if (bSetMovementModesFromZone && TargetZone)
		{
			MovementMode = TargetZone->GetPhysics();
			CustomMovementMode = TargetZone->GetCustomPhysics();
		}

		//Create transition
		auto* Transition = SourceZone->RegisterTransition(WorldSource, WorldTarget, CharacterClass, MovementMode, TargetZone, CustomMovementMode);
		if (!Transition)
		{
			continue;
		}

		//Set transition arguments
		Transition->bNeedsJump = bRequiresJump;
		if (!bAutoCalculateDirection)
		{
			Transition->TransitionDirection = LocalTransitionDirection;
		}
	}
}
