// Copyright 2015 Pascal Krabbe

#include "AdvKitRuntime.h"

#include "Environment/Transitions/AdvKitManualTransitionArea.h"
#include "Environment/Transitions/AdvKitTransitionComponentArea.h"
#include "Environment/AdvKitZone.h"

void UAdvKitManualTransitionArea::CreateTransition()
{
	auto* SourceZone = Cast<AAdvKitZone>(GetOuter());
	if (!SourceZone)
	{
		return;
	}

	//Transform local positions to world space
	FVector WorldSourceMin = SourceZone->GetTransform().TransformPositionNoScale(LocalSourceMin);
	FVector WorldSourceMax = SourceZone->GetTransform().TransformPositionNoScale(LocalSourceMax);
	if (bSnapSource)
	{
		WorldSourceMin = SourceZone->ConstrainPositionToZone(WorldSourceMin, FVector::ZeroVector);
		WorldSourceMax = SourceZone->ConstrainPositionToZone(WorldSourceMax, FVector::ZeroVector);
	}

	//Might not have a target zone, so use source instead
	FVector WorldTargetMin = SourceZone->GetTransform().TransformPositionNoScale(LocalTargetMin);
	FVector WorldTargetMax = SourceZone->GetTransform().TransformPositionNoScale(LocalTargetMax);
	if (bSnapSource)
	{
		WorldTargetMin = SourceZone->ConstrainPositionToZone(WorldTargetMin, FVector::ZeroVector);
		WorldTargetMax = SourceZone->ConstrainPositionToZone(WorldTargetMax, FVector::ZeroVector);
	}


	//Transform target locations into world space if target zone is set
	if (TargetZone)
	{
		WorldTargetMin = TargetZone->GetTransform().TransformPositionNoScale(LocalTargetMin);
		WorldTargetMax = TargetZone->GetTransform().TransformPositionNoScale(LocalTargetMax);

		if (bSnapSource)
		{
			WorldTargetMin = TargetZone->ConstrainPositionToZone(WorldTargetMin, FVector::ZeroVector);
			WorldTargetMax = TargetZone->ConstrainPositionToZone(WorldTargetMax, FVector::ZeroVector);
		}
	}

	//Create transitions for each configured character
	for (auto CharacterClass : UseableBy)
	{
		if (!IsValid(CharacterClass))
		{
			continue;
		}

		//Set transition movement modes
		auto MovementMode = TargetMovementMode;
		auto CustomMovementMode = TargetCustomMovementMode;

		if (bSetMovementModesFromZone && TargetZone)
		{
			MovementMode = TargetZone->GetPhysics();
			CustomMovementMode = TargetZone->GetCustomPhysics();
		}

		//Create transition
		auto* Transition = SourceZone->RegisterTransitionArea(WorldSourceMin, WorldTargetMin, WorldSourceMax, WorldTargetMax, CharacterClass, MovementMode, TargetZone, CustomMovementMode);
		if (!Transition)
		{
			continue;
		}

		//Set transition arguments
		Transition->bNeedsJump = bRequiresJump;
		if (!bAutoCalculateDirection)
		{
			Transition->TransitionDirection = LocalTransitionDirection;
		}
	}
}
