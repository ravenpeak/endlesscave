// Copyright 2015 Pascal Krabbe

#include "AdvKitRuntime.h"
#include "Environment/AdvKitTransitionComponent.h"
#include "Player/AdvKitCharacter.h"

FLinearColor UAdvKitTransitionComponent::StaticTransitionColor = FColorList::Grey;
FLinearColor UAdvKitTransitionComponent::DynamicTransitionColor = FColorList::MandarianOrange;

UAdvKitTransitionComponent::UAdvKitTransitionComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bIsDynamic = false;
	bNeedsJump = false;
	TransitionDirection = FVector::UpVector;
	MinDirectionNormal = 0.99f;
	MinLocationRadius = 2.0f;
}

FVector UAdvKitTransitionComponent::GetClosestSourcePosition(const FVector& ToWorldPosition)
{
	//Dummy implementation, implement in subclass
	return FVector::ZeroVector;
}

FVector UAdvKitTransitionComponent::GetClosestTargetPosition(const FVector& ToWorldPosition)
{
	//Dummy implementation, implement in subclass
	return FVector::ZeroVector;
}

bool UAdvKitTransitionComponent::CanTransition(AAdvKitCharacter* Character, const FVector& AtLocation, const FVector& InDirection, bool bIsJump)
{
	if (IsValid(UseableBy) && IsValid(Character))
	{
		if (!Character->IsA(UseableBy))
		{
			return false;
		}
	}

	if (bIsDynamic && !IsDynamicTransitionValid())
	{
		return false;
	}

	if (bNeedsJump != bIsJump)
	{
		return false;
	}

	auto* SourceZone = Cast<AAdvKitZone>(GetOuter());
	if (!SourceZone)
	{
		return false;
	}

	FVector GlobalDirection = SourceZone->GetTransform().TransformVectorNoScale(TransitionDirection);
	float Dot = FVector::DotProduct(InDirection.GetSafeNormal(), GlobalDirection.GetSafeNormal());
	if (Dot < MinDirectionNormal)
	{
		return false;
	}


	FVector ClosestStart = GetClosestSourcePosition(AtLocation);
	if (FVector::Dist(AtLocation, ClosestStart) > MinLocationRadius)
	{
		return false;
	}

	return true;
}

bool UAdvKitTransitionComponent::IsDynamicTransitionValid()
{
	//Dummy implementation, implement in subclass
	return true;
}

class UArrowComponent* UAdvKitTransitionComponent::CreateArrow(const FVector& WorldSource, const FVector& WorldTarget, AActor* OptionalBase)
{
	FName TransitionArrowName("TransitionArrow");

	AActor* ArrowBase = IsValid(OptionalBase) ? OptionalBase : Cast<AActor>(GetOuter());
	if (!IsValid(ArrowBase))
	{
		return NULL;
	}

	auto NewArrow = Cast<UArrowComponent>(ArrowBase->CreateComponentFromTemplate(Cast<UArrowComponent>(UArrowComponent::StaticClass()->GetDefaultObject())));
	if (!NewArrow)
	{
		return NULL;
	}

	ArrowBase->BlueprintCreatedComponents.Add(NewArrow);
	
	NewArrow->CreationMethod = EComponentCreationMethod::UserConstructionScript;

	NewArrow->ArrowSize = 1;
	NewArrow->SetRelativeScale3D(FVector((FVector::Dist(WorldSource, WorldTarget) / 80.0f), 1, 1));
	NewArrow->SetArrowColor_New(UAdvKitTransitionComponent::StaticTransitionColor);
	if (bIsDynamic)
	{
		NewArrow->SetArrowColor_New(UAdvKitTransitionComponent::DynamicTransitionColor);
	}

	NewArrow->SetWorldRotation((WorldTarget - WorldSource).Rotation());
	NewArrow->SetWorldLocation(WorldSource);

	NewArrow->AttachTo(ArrowBase->GetRootComponent(), NAME_None, EAttachLocation::KeepWorldPosition);
	NewArrow->RegisterComponent();

	return NewArrow;
}
