// Copyright 2015 Pascal Krabbe

#include "AdvKitRuntime.h"

#include "Player/AdvKitCharacter.h"
#include "AdvKitTypes.h"

#include "Environment/AdvKitZone.h"
#include "Environment/AdvKitTransitionBuilderModule.h"

UAdvKitTransitionBuilderModule::UAdvKitTransitionBuilderModule(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	DefaultCharacterClasses.Add(AAdvKitCharacter::StaticClass());
	ErrorMarginPosition = 3.0f;
	ErrorMarginDot = 0.01f;
}

void UAdvKitTransitionBuilderModule::CreateTransitions_Implementation(TArray<AAdvKitZone*>& OutAffectedZones)
{
	auto Zone = GetOuterZone();
	if (!Zone)
	{
		return;
	}

	for(auto CharacterClass : DefaultCharacterClasses)
	{ 
		if (!CharacterClass)
		{
			continue;
		}

		if (!CanCreateTransitionsFor(CharacterClass, Zone))
		{
			UE_LOG(LogAdvKit, Error, TEXT("%s cannot create transitions for zone %s with character %s!"), *GetName(), *Zone->GetName(), *CharacterClass->GetName());
			continue;
		}

		TArray<AAdvKitZone*> AffectedZones;
		CreateTransitionsFor(CharacterClass, Zone, AffectedZones);
		for (auto AffectedZone : AffectedZones)
		{
			OutAffectedZones.AddUnique(AffectedZone);
		}
	}

}

AAdvKitZone* UAdvKitTransitionBuilderModule::GetOuterZone()
{
	return GetTypedOuter<AAdvKitZone>();
}

bool UAdvKitTransitionBuilderModule::CanCreateTransitionsFor_Implementation(TSubclassOf<AAdvKitCharacter> ForCharacterClass, AAdvKitZone* ForZone)
{
	//Dummy implementation, override in subclass.
	return false;
}

void UAdvKitTransitionBuilderModule::CreateTransitionsFor_Implementation(TSubclassOf<AAdvKitCharacter> ForCharacterClass, AAdvKitZone* ForZone, TArray<AAdvKitZone*>& OutAffectedZones)
{
	//Gather potential targets
	TArray<AAdvKitZone*> TargetZones;
	if (!GatherPotentialTargetZones_Implementation(ForCharacterClass, ForZone, TargetZones))
	{
		//No targets means there can be no transitions
		return;
	}

	//Try to create transitions for all potential targets
	for (auto TargetZone : TargetZones)
	{
		if (CreateTransitionBetween_Implementation(ForCharacterClass, ForZone, TargetZone))
		{
			//Created transitions to this target, so the zone was affected
			OutAffectedZones.AddUnique(TargetZone);
		}
	}
}

bool UAdvKitTransitionBuilderModule::GatherPotentialTargetZones_Implementation(TSubclassOf<AAdvKitCharacter> ForCharacterClass, AAdvKitZone* ForZone, TArray<AAdvKitZone*>& OutZones)
{
	//Dummy implementation, override in subclass.
	OutZones.Empty();
	return false;
}

bool UAdvKitTransitionBuilderModule::CreateTransitionBetween_Implementation(TSubclassOf<AAdvKitCharacter> ForCharacterClass, AAdvKitZone* SourceZone, AAdvKitZone* TargetZone)
{
	//Dummy implementation, override in subclass.
	return false;
}

const FVector UAdvKitTransitionBuilderModule::GetCharacterHalfExtent(TSubclassOf<AAdvKitCharacter> ForCharacterClass, AAdvKitZone* ForZone) const
{
	check(ForCharacterClass);
	check(ForZone);

	auto DefaultCharacter = GetDefaultCharacterObject(ForCharacterClass);
	check(DefaultCharacter);

	auto DummyMoveComp = DefaultCharacter->GetAdvMovementComponent();
	check(DummyMoveComp);

	return DummyMoveComp->GetHalfExtentForZone(ForZone);
}

const FAdvKitMovementProperties* UAdvKitTransitionBuilderModule::GetCharacterMovementProperties(TSubclassOf<AAdvKitCharacter> ForCharacterClass) const
{
	auto DefaultCharacter = GetDefaultCharacterObject(ForCharacterClass);
	check(DefaultCharacter);

	return DefaultCharacter->GetAdvMovementProperties();
}

const FAdvKitMovementProperties& UAdvKitTransitionBuilderModule::GetCharacterMovementProperties_BP(TSubclassOf<AAdvKitCharacter> ForCharacterClass) const
{
	auto DefaultCharacter = GetDefaultCharacterObject(ForCharacterClass);
	check(DefaultCharacter);

	return *DefaultCharacter->GetAdvMovementProperties();
}

AAdvKitCharacter* UAdvKitTransitionBuilderModule::GetDefaultCharacterObject(TSubclassOf<AAdvKitCharacter> ForCharacterClass) const
{
	check(ForCharacterClass);

	//Not sure if this can ever happen, but I am a pessimist :P
	AAdvKitCharacter* DefaultCharacter = ForCharacterClass.GetDefaultObject();
	if (!DefaultCharacter)
	{
		UE_LOG(LogAdvKit, Error, TEXT("UAdvKitTransitionBuilderModule::GetDefaultCharacterObject Given character class does not have default object!"));
		return nullptr;
	}

	return DefaultCharacter;
}


bool UAdvKitTransitionBuilderModule::OverlapForCloseZones(class AAdvKitZone* ToZone, FVector OverlapStart, FVector OverlapEnd, float OverlapRadius, TArray<AAdvKitZone*>& OutOtherZones)
{
	static const float Half = 0.5f;

	//Cannot overlap without the zone or the engine
	if (!ToZone || !GEngine)
	{
		return false;
	}

	//Get world for overlap check
	UWorld* World = GEngine->GetWorldFromContextObject(ToZone);
	if (!World)
	{
		return false;
	}

	//Params for overlap
	TArray<FOverlapResult> OverlapResults;
	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(ToZone);
	FCollisionObjectQueryParams ObjectQueryParams;

	//Create overlap box
	FVector LocalStart = ToZone->GetTransform().InverseTransformPositionNoScale(OverlapStart);
	FVector LocalEnd = ToZone->GetTransform().InverseTransformPositionNoScale(OverlapEnd);
	FVector BoxExtent = (LocalEnd - LocalStart)*Half + FVector(1, 1, 1)*OverlapRadius;
	FVector BoxLocation = FMath::Lerp<FVector>(OverlapStart, OverlapEnd, Half);

	//Execute overlap
	if (!World->OverlapMultiByObjectType(OverlapResults, BoxLocation, ToZone->GetActorQuat(), ObjectQueryParams, FCollisionShape::MakeBox(BoxExtent), QueryParams))
	{
		//Nothing was found
		return false;
	}

	//Filter zones from overlap results
	for (FOverlapResult Result : OverlapResults)
	{
		if (!Result.Actor.IsValid())
		{
			//Only actors can be zones
			continue;
		}

		if(!Result.Actor->IsA<AAdvKitZone>())
		{
			//Not a zone
			continue;
		}

		//Add zone to result
		OutOtherZones.AddUnique(Cast<AAdvKitZone>(Result.Actor.Get()));
	}

	//Return if any zones were found
	return (OutOtherZones.Num() != 0);
}