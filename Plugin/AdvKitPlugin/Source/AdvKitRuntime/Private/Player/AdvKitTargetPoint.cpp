#include "AdvKitRuntime.h"
#include "Player/AdvKitTargetPoint.h"

AAdvKitTargetPoint::AAdvKitTargetPoint(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bCanBeTargeted = true;
}

AActor* AAdvKitTargetPoint::GetTargetedActor()
{
	//Check if target actor is attached to another actor to avoid targeting loose traget points
	if (!ParentComponentActor.IsValid())
	{
		return nullptr;
	}

	return ParentComponentActor.Get();
}

bool AAdvKitTargetPoint::IsValidTarget()
{
	return bCanBeTargeted;
}
