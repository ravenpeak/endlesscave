// Copyright 2015 Pascal Krabbe

#include "AdvKitRuntime.h"
#include "Player/AdvKitCharacterMovementComponent.h"
#include "Player/AdvKitOrientationComponent.h"
#include "Player/AdvKitTraceUtilityComponent.h"
#include "Player/AdvKitCharacter.h"
#include "Player/AdvKitCharacterActionManager.h"

#include "Environment/AdvKitZone.h"
#include "Environment/AdvKitZoneLocation.h"

#include "Environment/AdvKitTransitionComponent.h"
#include "Actions/Movement/AdvKitCharacterActionPhysics.h"


#include "Actions/Movement/AdvKitCA_Transition.h"

//Begin CharacterMovementComponent.cpp / Wrapper
#include "Navigation/PathFollowingComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogAdvKitCharacterMovement, Log, All);

// MAGIC NUMBERS from CharacterMovementComponent.cpp, works for epic, so it whould work for this
const float MAX_STEP_SIDE_Z = 0.08f;	// maximum z value for the normal on the vertical side of steps
const float SWIMBOBSPEED = -80.f;

//End CharacterMovementComponent.cpp  / Wrapper

#if WITH_EDITOR

FString GetMovementModeDisplayName(EAdvKitMovementMode EnumValue)
{
	const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EAdvKitMovementMode"), true);
	if (!EnumPtr) return FString("Invalid");

	return EnumPtr->GetEnumName((int32)EnumValue);
}
#else

FString GetMovementModeDisplayName(EAdvKitMovementMode EnumValue)
{
	return FString("Error: Editor Only");
}

#endif 


UAdvKitCharacterMovementComponent::UAdvKitCharacterMovementComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	NavAgentProps.bCanCrouch = true;
	NavAgentProps.bCanSwim = true;

	BalanceChangeTime = 1;
	MaxBalance = 1;
	CounterBalanceMultiplier = 1.5f;
	MinCounterBalanceDot = 0.25f;
	MaxBalanceTurnAroundDot = 0.7f;

	MinBalanceChangePerSecond = 0.8f;
	MaxBalanceChangePerSecond = 1.0f;

	bIgnoreBaseRotation = false;
}

void UAdvKitCharacterMovementComponent::SetMovementVariables(FVector NewVelocity, FVector NewAcceleration, FVector NewLocalAcceleration)
{
	if (!LocalAcceleration.IsNearlyZero())
	{
		LastNonZeroLocalAcceleration = LocalAcceleration;
	}

	if (!Acceleration.IsNearlyZero())
	{
		LastNonZeroAcceleration = Acceleration;
	}

	Acceleration = NewAcceleration;
	LocalAcceleration = NewLocalAcceleration;
	Velocity = NewVelocity;
}

void UAdvKitCharacterMovementComponent::ZeroMovementVariables()
{
	SetMovementVariables(FVector::ZeroVector, FVector::ZeroVector, FVector::ZeroVector);
}


void UAdvKitCharacterMovementComponent::TickComponent(float DeltaSeconds, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	if (!HasValidData() || UpdatedComponent->IsSimulatingPhysics())
	{
		// Movement is disabled, don't queue up actions that would apply when it is enabled again.
		ConsumeLocalInputVector();
		return;
	}

	if (CharacterOwner->Role > ROLE_SimulatedProxy)
	{
		// Allow root motion to move characters that have no controller.
		if (CharacterOwner->IsLocallyControlled() || bRunPhysicsWithNoController || (!CharacterOwner->Controller && CharacterOwner->IsPlayingRootMotion()))
		{
			LocalAcceleration = ScaleInputAcceleration(GetLocalInputVector());
			ConsumeLocalInputVector();
		}
	}

	SetMovementVariables(Velocity, Acceleration, LocalAcceleration); //Update movement variables

	Super::TickComponent(DeltaSeconds, TickType, ThisTickFunction);
}

float UAdvKitCharacterMovementComponent::GetMaxSpeed() const
{
	auto const MovementProps = GetAdvMovementProperties();
	check(MovementProps);

	switch (GetAdvMovementMode())
	{
	case EAdvKitMovementMode::ClimbingLedge:
		return MovementProps->LedgeClimbSpeed;
	case EAdvKitMovementMode::ClimbingWall:
		return MovementProps->WallClimbSpeed;
	case EAdvKitMovementMode::ClimbingLadder:
		return MovementProps->LadderClimbSpeed;
	case EAdvKitMovementMode::ClimbingCeiling:
		return MovementProps->CeilingClimbSpeed;
	case EAdvKitMovementMode::WalkingTightspace:
		return MovementProps->TightSpaceWalkSpeed;
	case EAdvKitMovementMode::WalkingBalance:
		return MovementProps->BalanceWalkSpeed;
	}

	return Super::GetMaxSpeed();
}

void UAdvKitCharacterMovementComponent::SetUpdatedComponent(USceneComponent* NewUpdatedComponent)
{
	Super::SetUpdatedComponent(NewUpdatedComponent);
	AdvKitCharacterOwner = Cast<AAdvKitCharacter>(PawnOwner);
}

void UAdvKitCharacterMovementComponent::SetAdvMovementMode(EAdvKitMovementMode NewMovementMode, uint8 NewCustomMode)
{
	switch (NewMovementMode)
	{
	//Wrap old movement modes
	case EAdvKitMovementMode::None:
		SetMovementMode(MOVE_None);
		break;
	case EAdvKitMovementMode::Walking:
		SetMovementMode(MOVE_Walking);
		break;
	case EAdvKitMovementMode::NavWalking:
		SetMovementMode(MOVE_NavWalking);
		break;
	case EAdvKitMovementMode::Falling:
		SetMovementMode(MOVE_Falling);
		break;
	case EAdvKitMovementMode::Swimming:
		SetMovementMode(MOVE_Swimming);
		break;
	case EAdvKitMovementMode::Flying:
		SetMovementMode(MOVE_Flying);
		break;
	//Utilize custom movement mode for new modes
	default:
		SetMovementMode(MOVE_Custom, (uint8)NewMovementMode + NewCustomMode);
		break;
	}
}

EAdvKitMovementMode UAdvKitCharacterMovementComponent::GetAdvMovementMode() const
{
	//Convert movement mode to advkit movement mode
	if (MovementMode == MOVE_Custom)
	{
		if (CustomMovementMode >= (uint8)EAdvKitMovementMode::Custom)
		{
			return EAdvKitMovementMode::Custom;
		}

		return (EAdvKitMovementMode)CustomMovementMode;
	}

	return (EAdvKitMovementMode)MovementMode.GetValue();
}

uint8 UAdvKitCharacterMovementComponent::GetAdvCustomMovementMode() const
{
	if (CustomMovementMode >= (uint8)EAdvKitMovementMode::Custom)
	{
		return CustomMovementMode - (uint8)EAdvKitMovementMode::Custom;
	}

	return 0;
}

bool UAdvKitCharacterMovementComponent::IsClimbingWall() const
{
	if (!HasValidData())
	{
		return false;
	}

	return GetAdvMovementMode() == EAdvKitMovementMode::ClimbingWall;
}

bool UAdvKitCharacterMovementComponent::IsClimbingCeiling() const
{
	if (!HasValidData())
	{
		return false;
	}

	return GetAdvMovementMode() == EAdvKitMovementMode::ClimbingCeiling;
}

bool UAdvKitCharacterMovementComponent::IsClimbingLadder() const
{
	if (!HasValidData())
	{
		return false;
	}

	return GetAdvMovementMode() == EAdvKitMovementMode::ClimbingLadder;
}

bool UAdvKitCharacterMovementComponent::IsClimbingLedge() const
{
	if (!HasValidData())
	{
		return false;
	}

	return GetAdvMovementMode() == EAdvKitMovementMode::ClimbingLedge;
}

bool UAdvKitCharacterMovementComponent::IsWalkingTightspace() const
{
	if (!HasValidData())
	{
		return false;
	}

	return GetAdvMovementMode() == EAdvKitMovementMode::WalkingTightspace;
}

bool UAdvKitCharacterMovementComponent::IsWalkingBalance() const
{
	if (!HasValidData())
	{
		return false;
	}

	return GetAdvMovementMode() == EAdvKitMovementMode::WalkingBalance;

}

bool UAdvKitCharacterMovementComponent::LetGoOfZone()
{
	//Cannot let go of zone if not in zone
	auto* LocationInZone = GetZoneLocation();
	if (!LocationInZone)
	{
		return false;
	}

	//Letting go of these zones can only mean falling off
	if (IsClimbingLadder() || IsClimbingWall() || IsClimbingCeiling())
	{
		return AdvKitCharacterOwner->StartTransitionActionTo(EAdvKitMovementMode::Falling, UAdvKitCA_Transition::MakeArguments((AAdvKitZone*)nullptr));
	}

	//These zones support downwards transitions
	if ((IsWalkingTightspace() || IsClimbingLedge()) && LocationInZone)
	{
		return TryToTransition(
			GetZone(),
			LocationInZone->GetGlobalPosition(),
			-AdvKitCharacterOwner->GetActorUpVector()*JumpZVelocity
			);
	}

	return false;
}

bool UAdvKitCharacterMovementComponent::JumpUpZone()
{
	switch (GetAdvMovementMode())
	{
	case EAdvKitMovementMode::ClimbingWall:
		//Nothing done, but this will prevent jumping
		return true;
	case EAdvKitMovementMode::ClimbingLadder:
	case EAdvKitMovementMode::ClimbingLedge:
	{
		//Try to transition upwards from zone
		auto* LocationInZone = GetZoneLocation();
		if (!LocationInZone)
		{
			return false;
		}

		FVector TransitionDirection = Acceleration;
		if (TransitionDirection.IsNearlyZero())
		{
			TransitionDirection = CharacterOwner->GetActorUpVector()*JumpZVelocity;
		}

		return TryToTransition(
			GetZone(),
			LocationInZone->GetGlobalPosition(),
			TransitionDirection,
			true
			);
	}
	break;
	}

	return false;
}

bool UAdvKitCharacterMovementComponent::TryToTransition(AAdvKitZone* ForZone, const FVector& ForWorldPosition, const FVector& InWorldDirection, bool bIsJump)
{
	//Try to find a transition
	UAdvKitTransitionComponent* ClosestTransition = GetPossibleTransition(ForZone, ForWorldPosition, InWorldDirection, bIsJump);
	if (ClosestTransition == nullptr)
	{
		return false;
	}

	//Execute the transition
	return AdvKitCharacterOwner->StartTransitionActionTo(ClosestTransition->TargetPhysics, UAdvKitCA_Transition::MakeArguments(ClosestTransition));
}

UAdvKitTransitionComponent* UAdvKitCharacterMovementComponent::GetPossibleTransition(AAdvKitZone* ForZone, const FVector& ForWorldPosition, const FVector& InWorldDirection, bool bIsJump)
{
	//Get all transitions for this character
	TArray<UAdvKitTransitionComponent*> AvailableTransitions = ForZone->GetPossibleTransitions(AdvKitCharacterOwner);
	if (AvailableTransitions.Num() == 0)
	{
		return nullptr;
	}

	TArray<UAdvKitTransitionComponent*> PossibleTransitions;

	//Filter out transitions that are traversable with the arguments
	for (auto& TempTransition : AvailableTransitions)
	{
		if(!IsValid(TempTransition))
		{
			continue;
		}

		if (!TempTransition->CanTransition(AdvKitCharacterOwner, ForWorldPosition, InWorldDirection, bIsJump))
		{
			continue;
		}

		PossibleTransitions.Add(TempTransition);
	}

	//Cannot do anything without transitions
	if (PossibleTransitions.Num() == 0)
	{
		return nullptr;
	}

	//Find the closest transition and return it
	float ClosestDistance = 0;
	UAdvKitTransitionComponent* ClosestTransition = nullptr;

	for (auto& TempTransition : PossibleTransitions)
	{
		if (!IsValid(TempTransition))
		{
			continue;
		}

		float Distance = FVector::Dist(ForWorldPosition, TempTransition->GetClosestSourcePosition(ForWorldPosition));
		if (!ClosestTransition || ClosestDistance > Distance)
		{
			ClosestDistance = Distance;
			ClosestTransition = TempTransition;
		}
	}

	return ClosestTransition;
}

bool UAdvKitCharacterMovementComponent::CanTransition(AAdvKitZone* ForZone, const FVector& ForWorldPosition, const FVector& InWorldDirection, bool bIsJump)
{
	//At least one transition can be used
	return GetPossibleTransition(ForZone, ForWorldPosition, InWorldDirection, bIsJump) != nullptr;
}

void UAdvKitCharacterMovementComponent::StartNewPhysics(float DeltaSeconds, int32 Iterations)
{
	if ((DeltaSeconds < 0.0003f) || (Iterations > 7) || !HasValidData())
		return;

	if (UpdatedComponent->IsSimulatingPhysics())
	{
		UE_LOG(LogAdvKitCharacterMovement, Log, TEXT("UCharacterMovementComponent::StartNewPhysics: UpdateComponent (%s) is simulating physics - aborting."), *UpdatedComponent->GetPathName());
		return;
	}

	bMovementInProgress = true;

	//Start new physics depending on mode
	switch (GetAdvMovementMode())
	{
	case EAdvKitMovementMode::None:
		return;
	case EAdvKitMovementMode::Walking:
		PhysWalking(DeltaSeconds, Iterations);
		break;
	case EAdvKitMovementMode::NavWalking:
		PhysNavWalking(DeltaSeconds, Iterations);
		break;
	case EAdvKitMovementMode::Falling:
		PhysFalling(DeltaSeconds, Iterations);
		break;
	case EAdvKitMovementMode::Swimming:
		PhysSwimming(DeltaSeconds, Iterations);
		break;
	case EAdvKitMovementMode::Flying:
		PhysFlying(DeltaSeconds, Iterations);
		break;
	case EAdvKitMovementMode::ClimbingWall:
		PhysClimbWall(DeltaSeconds, Iterations);
		break;
	case EAdvKitMovementMode::ClimbingCeiling:
		PhysClimbCeiling(DeltaSeconds, Iterations);
		break;
	case EAdvKitMovementMode::ClimbingLedge:
		PhysClimbLedge(DeltaSeconds, Iterations);
		break;
	case EAdvKitMovementMode::ClimbingLadder:
		PhysClimbLadder(DeltaSeconds, Iterations);
		break;
	case EAdvKitMovementMode::WalkingTightspace:
		PhysTightSpace(DeltaSeconds, Iterations);
		break;
	case EAdvKitMovementMode::WalkingBalance:
		PhysBalancing(DeltaSeconds, Iterations);
		break;
	case EAdvKitMovementMode::ActionDriven:
		PhysActionDriven(DeltaSeconds, Iterations);
		break;
	case EAdvKitMovementMode::Custom:
	default:
		PhysCustom(DeltaSeconds, Iterations);
		break;
	}

	bMovementInProgress = false;

	if (bDeferUpdateMoveComponent)
	{
		SetUpdatedComponent(DeferredUpdatedMoveComponent);
	}
}

void UAdvKitCharacterMovementComponent::PhysWalking(float DeltaSeconds, int32 Iterations)
{	
	//Test if character is walking over a ledge by first tracing in the walking direction and down
	//to make sure there is enough room beneath the edge. Lastly trace backwards to check if a ledge
	//to grab exists
	FHitResult AbyssHit;
	const float SmallHeightOffset = GetAdvMovementProperties()->LedgeSmallHeightOffset;
	const float DownTraceDistance = GetAdvMovementProperties()->LedgeDownTraceDistance;
	const float LedgePrecognitionDistance = GetAdvMovementProperties()->LedgePrecognitionDistance;

	FVector CapsuleExtent = GetPawnCapsuleExtent(EShrinkCapsuleExtent::SHRINK_None);

	FVector UpVector = UpdatedComponent->GetUpVector();
	FVector ForwardVector = UpdatedComponent->GetForwardVector();

	FVector AbyssStart = UpdatedComponent->GetComponentLocation();
	AbyssStart -= UpVector*CapsuleExtent.Z;
	AbyssStart += UpVector*SmallHeightOffset;
	AbyssStart += Velocity.GetSafeNormal() * LedgePrecognitionDistance;
	AbyssStart += Velocity*DeltaSeconds;
	
	FVector AbyssEnd = AbyssStart - UpVector*DownTraceDistance;

	//Check if below is empty
	FCollisionQueryParams Params;
	FCollisionObjectQueryParams ObjectParams;
	if (!GetWorld()->LineTraceSingleByObjectType(AbyssHit, AbyssStart, AbyssEnd, ObjectParams, Params))
	{
		FVector LedgeTraceEnd = UpdatedComponent->GetComponentLocation();
		LedgeTraceEnd -= UpVector*CapsuleExtent.Z;

		//Check if a ledge exists
		FHitResult LedgeHit;
		if (GetWorld()->LineTraceSingleByObjectType(LedgeHit, AbyssEnd, LedgeTraceEnd, ObjectParams, Params))
		{
			auto Zone = Cast<AAdvKitZone>(LedgeHit.GetActor());
			if (Zone)
			{
				//Check if zone is a ledge
				if (Zone->GetPhysics() == EAdvKitMovementMode::ClimbingLedge)
				{
					//Use ledge
					if (AdvKitCharacterOwner->StartTransitionActionTo(Zone->GetPhysics(), UAdvKitCA_Transition::MakeArguments(Zone)))
					{
						return;
					}
				}
			}
		}
	}

	Super::PhysWalking(DeltaSeconds, Iterations);
	return;
}

float UAdvKitCharacterMovementComponent::ComputeAnalogInputModifier() const
{
	const float ModifiedMaxAccel = GetMaxAcceleration();
	if ((Acceleration.SizeSquared() > 0.f || LocalAcceleration.SizeSquared() > 0.f) && ModifiedMaxAccel > SMALL_NUMBER) //Changed
	{
		return FMath::Clamp(FMath::Max<float>(Acceleration.Size(), LocalAcceleration.Size()) / ModifiedMaxAccel, 0.f, 1.f); //Changed
	}

	return 0.f;
}

void UAdvKitCharacterMovementComponent::PhysClimbWall(float DeltaSeconds, int32 Iterations)
{
	auto* LocationInZone = GetZoneLocation();
	FVector ZoneAcceleration = FVector::ZeroVector;

	if (LocationInZone && !LocalAcceleration.IsNearlyZero())
	{
		//Move to the right 
		ZoneAcceleration = LocalAcceleration.Y * CharacterOwner->GetActorRightVector();

		//Move upwards when player goes forward
		ZoneAcceleration.Z = LocalAcceleration.X;

		//Constrain direction to zone
		ZoneAcceleration = GetZone()->ConstrainDirectionToZone(LocationInZone->ConstrainDirection(ZoneAcceleration).GetSafeNormal() * AdvMovementProps.WallClimbSpeed, LocationInZone->GetGlobalPosition());
	}

	FVector Offset = GetDesiredOffsetFromZone(GetZone(), LocationInZone);
	FVector Extent = GetHalfExtentForZone(GetZone());
	PhysMoveInZone(ZoneAcceleration, Offset, DeltaSeconds, Extent, true);
}

void UAdvKitCharacterMovementComponent::PhysClimbCeiling(float DeltaSeconds, int32 Iterations)
{
	auto* LocationInZone = GetZoneLocation();
	FVector ZoneAcceleration = FVector::ZeroVector;

	//Constrain direction to zone
	if (LocationInZone && !Acceleration.IsNearlyZero())
	{
		ZoneAcceleration = LocationInZone->ConstrainDirection(Acceleration).GetSafeNormal() * AdvMovementProps.CeilingClimbSpeed;
	}

	FVector Offset = GetDesiredOffsetFromZone(GetZone(), LocationInZone);
	FVector Extent = GetHalfExtentForZone(GetZone());
	FVector OldLocation = GetActorLocation();
	
	//Move in zone
	if (!PhysMoveInZone(ZoneAcceleration, Offset, DeltaSeconds, Extent, true))
	{
		return;
	}

	if (!LocationInZone)
	{
		return;
	}

	//If the character has not moved as it should have, but still moved it might be because it was constrained by the zone's borders
	FVector DeltaLocation = GetActorLocation() - OldLocation;
	float MovementDot = FVector::DotProduct(DeltaLocation.GetSafeNormal(), ZoneAcceleration.GetSafeNormal());
	if (MovementDot < AdvMovementProps.CeilingMinTransitionDot)
	{
		TryToTransition(
			LocationInZone->Zone,
			LocationInZone->GetGlobalPosition(),
			ZoneAcceleration
			);
	}


}

void UAdvKitCharacterMovementComponent::PhysClimbLedge(float DeltaSeconds, int32 Iterations)
{
	auto* LocationInZone = GetZoneLocation();
	FVector ZoneAcceleration = FVector::ZeroVector;

	//Constrain direction to zone
	if (LocationInZone && !Acceleration.IsNearlyZero())
	{
		ZoneAcceleration = LocationInZone->ConstrainDirection(Acceleration).GetSafeNormal() * AdvMovementProps.LedgeClimbSpeed;
	}

	FVector Offset = GetDesiredOffsetFromZone(GetZone(), LocationInZone);
	FVector Extent = GetHalfExtentForZone(GetZone());
	PhysMoveInZone(ZoneAcceleration, Offset, DeltaSeconds, Extent, true);
}

void UAdvKitCharacterMovementComponent::PhysClimbLadder(float DeltaSeconds, int32 Iterations)
{
	FVector ZoneAcceleration = FVector::ZeroVector;

	auto* LocationInZone = GetZoneLocation();
	if (LocationInZone)
	{
		auto Zone = GetZone();
		ZoneAcceleration =
			//Go sideways with local side speed
			(LocalAcceleration.Y * -Zone->GetZoneRightVector(LocationInZone)).GetClampedToMaxSize(AdvMovementProps.LadderClimbSpeed)
			//Go upwards with forward speed
			+ (LocalAcceleration.X * Zone->GetZoneUpVector(LocationInZone)).GetClampedToMaxSize(AdvMovementProps.LadderClimbSpeed);

		LocalAcceleration = LocalAcceleration.GetClampedToMaxSize(AdvMovementProps.LadderClimbSpeed);
	}

	FVector Offset = GetDesiredOffsetFromZone(GetZone(), LocationInZone);
	FVector Extent = GetHalfExtentForZone(GetZone());
	PhysMoveInZone(ZoneAcceleration, Offset, DeltaSeconds, Extent, true, false);
}

void UAdvKitCharacterMovementComponent::PhysTightSpace(float DeltaSeconds, int32 Iterations)
{
	auto* LocationInZone = GetZoneLocation();
	FVector ZoneAcceleration = FVector::ZeroVector;

	//Constrain move aligning to zone
	if (LocationInZone && !Acceleration.IsNearlyZero())
	{
		ZoneAcceleration = LocationInZone->ConstrainDirection(Acceleration).GetSafeNormal() * AdvMovementProps.TightSpaceWalkSpeed;
	}

	FVector Offset = GetDesiredOffsetFromZone(GetZone(),LocationInZone);
	FVector Extent = GetHalfExtentForZone(GetZone());
	PhysMoveInZone(ZoneAcceleration, Offset, DeltaSeconds, Extent, true);
}

void UAdvKitCharacterMovementComponent::PhysBalancing(float DeltaSeconds, int32 Iterations)
{
	if ((!AdvKitCharacterOwner || !CharacterOwner->Controller) && !bRunPhysicsWithNoController && !HasRootMotion())
	{
		ZeroMovementVariables();
		return;
	}

	FVector ZoneAcceleration = FVector::ZeroVector;

	if (!HasRootMotion())
	{
		//Handle Balance change
		if (!GetWorld()->GetTimerManager().IsTimerActive(ChangeBalanceTimerHandle))
		{
			GetWorld()->GetTimerManager().SetTimer(ChangeBalanceTimerHandle,this, &UAdvKitCharacterMovementComponent::ChangeTargetBalance, BalanceChangeTime, false);
		}

		//Automatic imbalancing
		CurrentBalance += BalanceChangePerSecond*DeltaSeconds;

		//Player trying to re-balance
		float BalanceModificationNormalDot = FVector::DotProduct(UpdatedComponent->GetRightVector(), Acceleration.GetSafeNormal());
		if (FMath::Abs<float>(BalanceModificationNormalDot) > MinCounterBalanceDot)
		{
			CurrentBalance += BalanceModificationNormalDot * DeltaSeconds * CounterBalanceMultiplier;
		}

		//Falling off
		if (CurrentBalance >= MaxBalance || CurrentBalance <= -MaxBalance)
		{
			if (AdvKitCharacterOwner->StartTransitionActionTo(EAdvKitMovementMode::Falling, UAdvKitCA_Transition::MakeArguments((AAdvKitZone*)nullptr)))
			{
				return;
			}
		}

		ZoneAcceleration = Acceleration.GetClampedToMaxSize(AdvMovementProps.BalanceWalkSpeed);
	}

	auto* LocationInZone = GetZoneLocation();
	FVector Offset = GetDesiredOffsetFromZone(GetZone(), LocationInZone);
	FVector Extent = GetHalfExtentForZone(GetZone());
	PhysMoveInZone(ZoneAcceleration, Offset, DeltaSeconds, Extent, true, true);
}

void UAdvKitCharacterMovementComponent::PhysActionDriven(float DeltaSeconds, int32 Iterations)
{
	if (!AdvKitCharacterOwner)
	{
		return;
	}

	if (!AdvKitCharacterOwner->ActionManager)
	{
		return;
	}

	auto PhysicsAction = Cast<UAdvKitCharacterActionPhysics>(AdvKitCharacterOwner->ActionManager->GetActiveAction());
	if (!PhysicsAction)
	{
		UE_LOG(LogAdvKit,Error, TEXT("UAdvKitCharacterMovementComponent::PhysActionDriven: MovementMode is \"ActionDriven\", but no physics action is active!"));
		return;
	}

	PhysicsAction->ExecutePhysicsMovement(DeltaSeconds);
}

FVector UAdvKitCharacterMovementComponent::ConsumeLocalInputVector()
{
	FVector OldValue = LocalControlInputVector;
	LocalControlInputVector = FVector::ZeroVector;
	return OldValue;
}

void UAdvKitCharacterMovementComponent::AddLocalInputVector(FVector LocalVector, bool bForce)
{
	if (bForce || !IsMoveInputIgnored())
	{
		LocalControlInputVector += LocalVector;
	}
}

FVector UAdvKitCharacterMovementComponent::GetLocalInputVector() const
{
	return LocalControlInputVector;
}

void UAdvKitCharacterMovementComponent::PhysicsRotation(float DeltaSeconds)
{
	if (!HasValidData() || (!CharacterOwner->Controller && !bRunPhysicsWithNoController))
	{
		return;
	}

	const FRotator CurrentRotation = UpdatedComponent->GetComponentRotation();
	FRotator DeltaRot = GetDeltaRotation(DeltaSeconds);
	FRotator DesiredRotation = CurrentRotation;

	// Accumulate a desired new rotation.
	FRotator NewRotation = CurrentRotation;
	FVector DeltaMove = FVector::ZeroVector;

	switch (GetAdvMovementMode())
	{
	case EAdvKitMovementMode::None:
	case EAdvKitMovementMode::Walking:
	case EAdvKitMovementMode::NavWalking:
	case EAdvKitMovementMode::Falling:
	case EAdvKitMovementMode::ClimbingCeiling:
	case EAdvKitMovementMode::Swimming:
		Super::PhysicsRotation(DeltaSeconds);
		return;
	//These zones all have fixed orientations for the character
	case EAdvKitMovementMode::ClimbingWall:
	case EAdvKitMovementMode::WalkingTightspace:
	case EAdvKitMovementMode::ClimbingLedge:
	case EAdvKitMovementMode::ClimbingLadder:
	case EAdvKitMovementMode::WalkingBalance:
		NewRotation = GetDesiredRotationInZone(GetZone(), GetZoneLocation());
		break;
	case EAdvKitMovementMode::ActionDriven:
	{
		if (!AdvKitCharacterOwner)
		{
			return;
		}

		if (!AdvKitCharacterOwner->ActionManager)
		{
			return;
		}

		auto PhysicsAction = Cast<UAdvKitCharacterActionPhysics>(AdvKitCharacterOwner->ActionManager->GetActiveAction());
		if (!PhysicsAction)
		{
			UE_LOG(LogAdvKit,Error, TEXT("PhysicsRotation: MovementMode is \"ActionDriven\", but no physics action is active!"));
			return;
		}

		PhysicsAction->ExecutePhysicsRotation(DeltaSeconds);
		return;
	}
	break;
	case EAdvKitMovementMode::Custom:
		break;
	default:
		break;
	}

	// Set the new rotation.
	if (!NewRotation.Equals(CurrentRotation.GetDenormalized(), 0.01f))
	{
		MoveUpdatedComponent(DeltaMove, NewRotation, true);
	}
}

bool UAdvKitCharacterMovementComponent::DoJump(bool bReplayingMoves)
{
	if (GetZone())
	{
		return JumpUpZone();
	}

	return Super::DoJump(bReplayingMoves);
}

void UAdvKitCharacterMovementComponent::SetZone(class AAdvKitZone* NewZone)
{
	if (!AdvKitCharacterOwner)
	{
		return;
	}

	return AdvKitCharacterOwner->SetZone(NewZone);
}

void UAdvKitCharacterMovementComponent::SetZoneLocation(UAdvKitZoneLocation* NewLocation)
{
	if (!AdvKitCharacterOwner)
	{
		return;
	}

	AdvKitCharacterOwner->SetZoneLocation(NewLocation);
}

UAdvKitZoneLocation* UAdvKitCharacterMovementComponent::GetZoneLocation()
{
	if (!AdvKitCharacterOwner)
	{
		return nullptr;
	}

	return AdvKitCharacterOwner->GetZoneLocation();
}


class AAdvKitZone* UAdvKitCharacterMovementComponent::GetZone()
{
	if (!AdvKitCharacterOwner)
	{
		return nullptr;
	}

	return AdvKitCharacterOwner->GetZone();
}

bool UAdvKitCharacterMovementComponent::MoveLocationInZone(FVector GlobalAcceleration, float DeltaSeconds, const FVector& Extent, bool AutoTransition, bool ConstrainAcceleration)
{
	check(AdvKitCharacterOwner);

	//Needs a location to move
	auto* LocationInZone = AdvKitCharacterOwner->GetZoneLocation();
	if (!LocationInZone)
	{
		return false;
	}

	//DrawDebugCoordinateSystem(GetWorld(), LocationInZone->GetGlobalPosition(), UpdatedComponent->GetComponentRotation(), 100);

	FVector ActualAcceleration = ConstrainAcceleration ? LocationInZone->ConstrainDirection(GlobalAcceleration) : GlobalAcceleration;

	//Try to move location in the desired direction
	if (LocationInZone->MoveInDirection(ActualAcceleration*DeltaSeconds, Extent, UpdatedComponent->GetComponentRotation()))
	{
		return true;
	}

	//Location has not moved should a transition be attempted?
	if (!AutoTransition)
	{
		return false;
	}

	//If the character didn't move it is very likely it reached the end of the zone, 
	//it should try to transition when the player still wants to move
	if (GlobalAcceleration.IsNearlyZero())
	{
		return false;
	}

	TryToTransition(
		LocationInZone->Zone,
		LocationInZone->GetGlobalPosition(),
		ActualAcceleration
		);

	//It might transition but character has not moved
	return false;
}

FVector UAdvKitCharacterMovementComponent::GetDesiredOffsetFromZone(AAdvKitZone* Zone, UAdvKitZoneLocation* AtLocation)
{
	if (!AtLocation || !Zone)
	{
		return FVector::ZeroVector;
	}

	EAdvKitMovementMode ForMovementMode = Zone->GetPhysics();
	//uint8 ForCustomMovementMode = Zone->GetCustomPhysics();

	FVector LocalOffset = FVector::ZeroVector;

	switch (ForMovementMode)
	{
	case EAdvKitMovementMode::ClimbingLedge:
		LocalOffset = AdvMovementProps.LedgeOffset;
		break;
	case EAdvKitMovementMode::ClimbingWall:
		LocalOffset = AdvMovementProps.WallOffset;
		break;
	case EAdvKitMovementMode::ClimbingLadder:
		LocalOffset = AdvMovementProps.LadderOffset;
		break;
	case EAdvKitMovementMode::ClimbingCeiling:
		LocalOffset = AdvMovementProps.CeilingOffset;
		break;
	case EAdvKitMovementMode::WalkingTightspace:
		LocalOffset = AdvMovementProps.TightSpaceOffset;
		break;
	case EAdvKitMovementMode::WalkingBalance:
		LocalOffset = AdvMovementProps.BalanceOffset;
		break;
	default:
		break;
	}

	//Transform desired offsets into world space using the location orienation inside the current zone
	FVector OffsetFromZone =
		Zone->GetZoneForwardVector(AtLocation) * LocalOffset.X
		+ Zone->GetZoneRightVector(AtLocation) * LocalOffset.Y
		+ Zone->GetZoneUpVector(AtLocation) * LocalOffset.Z;


	return OffsetFromZone;
}

FRotator UAdvKitCharacterMovementComponent::GetDesiredRotationInZone(AAdvKitZone* Zone, UAdvKitZoneLocation* AtLocation)
{
	FRotator NewRotation = CharacterOwner->GetActorRotation();

	if (!AtLocation || !Zone)
	{
		return NewRotation;
	}

	FVector ZoneForward = Zone->GetZoneForwardVector(AtLocation);
	FVector ZoneRight = Zone->GetZoneRightVector(AtLocation);
	FVector ZoneUp = Zone->GetZoneUpVector(AtLocation);

	EAdvKitMovementMode ForMovementMode = Zone->GetPhysics();
	//uint8 ForCustomMovementMode = Zone->GetCustomPhysics();

	switch (ForMovementMode)
	{
	case EAdvKitMovementMode::WalkingTightspace:
		//Just look at the zone
		NewRotation = ZoneForward.Rotation();
		break;
	case EAdvKitMovementMode::ClimbingWall:
	case EAdvKitMovementMode::ClimbingLedge:
	case EAdvKitMovementMode::ClimbingLadder:
		{
			FQuat ZoneOrientation = FMatrix(
				ZoneForward,
				ZoneRight,
				ZoneUp,
				FVector::ZeroVector
				).Rotator().Quaternion();


			//Look at zone
			FRotator LocalLookAtRotation(0, 180, 0);
			//Convert to global rotation
			NewRotation = (ZoneOrientation * LocalLookAtRotation.Quaternion()).Rotator();
		}
		break;
	case EAdvKitMovementMode::WalkingBalance:
		{
			FVector GlobalForwardVector = CharacterOwner->GetActorForwardVector();
			FVector ZoneForwardVector = ZoneRight;

			//Find out which direction to face
			float DotProduct = FVector::DotProduct(ZoneForwardVector, LastNonZeroAcceleration.GetSafeNormal());
			if (DotProduct > MaxBalanceTurnAroundDot)
			{
				GlobalForwardVector = ZoneForwardVector;
			}
			else if (DotProduct < -MaxBalanceTurnAroundDot)
			{
				GlobalForwardVector = -ZoneForwardVector;
			}
			//Not going forwards or backwards, so the character should align the way it is already facing
			else
			{
				GlobalForwardVector = ZoneForwardVector*FVector::DotProduct(ZoneForwardVector, GlobalForwardVector);
			}

			GlobalForwardVector.Normalize();

			//Convert to global space
			FQuat GlobalDesiredQuat = GlobalForwardVector.Rotation().Quaternion();

			//Calculate delta degrees and lerp alpha
			FQuat CurrentQuat = UpdatedComponent->GetComponentQuat();
			float DegreeDifference = FMath::RadiansToDegrees<float>(FMath::Acos(FVector::DotProduct(GlobalDesiredQuat.GetAxisX(), CurrentQuat.GetAxisX())));
			float Alpha = FMath::FInterpConstantTo(0, DegreeDifference, GetWorld()->DeltaTimeSeconds, AdvMovementProps.BalanceRotationRate) / DegreeDifference;

			//Apply interpolation and set new rotation
			FQuat NewQuat = FQuat::Slerp(UpdatedComponent->GetComponentQuat(), GlobalDesiredQuat, Alpha);
			NewRotation = NewQuat.Rotator();
		}
		break;
	}

	return NewRotation;
}

FVector UAdvKitCharacterMovementComponent::GetHalfExtentForZone(AAdvKitZone* Zone)
{
	//GetPawnCapsuleExtent is not safe to be called on the default object
	auto TempOwner = Cast<AAdvKitCharacter>(GetOwner());
	check(TempOwner);

	float Radius, HalfHeight;
	TempOwner->GetCapsuleComponent()->GetScaledCapsuleSize(Radius, HalfHeight);
	FVector LocalExtent(Radius, Radius, HalfHeight);

	return LocalExtent;
}

bool UAdvKitCharacterMovementComponent::PhysMoveInZone(FVector GlobalAcceleration, FVector WorldOffset, float DeltaSeconds, const FVector& Extent, bool AutoTransition, bool ConstrainAcceleration)
{
	if ((!AdvKitCharacterOwner || !CharacterOwner->Controller) && !bRunPhysicsWithNoController && !HasRootMotion())
	{
		ZeroMovementVariables();
		return false;
	}

	bool bMoved = false;
	if (!HasRootMotion())
	{
		bMoved = MoveLocationInZone(GlobalAcceleration, DeltaSeconds, Extent, AutoTransition, ConstrainAcceleration);

		auto* LocationInZone = GetZoneLocation();
		if (!IsValid(LocationInZone))
		{
			return false;
		}

		FVector ComponentLocation = UpdatedComponent->GetComponentLocation();
		
		if (DeltaSeconds == 0)
		{
			return false;
		}

		//Velocity is calculated from delta location
		Velocity = (LocationInZone->GetGlobalPosition() + WorldOffset - ComponentLocation) / DeltaSeconds;
		if (!bMoved && !GlobalAcceleration.IsNearlyZero())
		{
			return false;
		}
	}

	//FHitResult Hit;
	//MoveUpdatedComponent(Velocity*DeltaSeconds, CharacterOwner->GetActorRotation(), true, &Hit);
	//if (Hit.bStartPenetrating && UpdatedComponent)
	//{
	//	PrintDebug(Hit.Actor->GetName());
	//}
	MoveUpdatedComponent(Velocity*DeltaSeconds, CharacterOwner->GetActorRotation(), false);
	
	return bMoved;
}

FVector UAdvKitCharacterMovementComponent::GetGlobalLocationInZone()
{
	auto* LocationInZone = GetZoneLocation();
	if (!LocationInZone)
	{
		return FVector::ZeroVector;
	}

	return LocationInZone->GetGlobalPosition();
}

void UAdvKitCharacterMovementComponent::ChangeTargetBalance()
{
	//Random new balance
	BalanceChangePerSecond = FMath::FRandRange(MinBalanceChangePerSecond, MaxBalanceChangePerSecond);
	if (FMath::RandRange(0, 1))
	{
		BalanceChangePerSecond *= -1;
	}
	
	//Restart Timer
	if (IsWalkingBalance())
	{
		if (!GetWorld()->GetTimerManager().IsTimerActive(ChangeBalanceTimerHandle))
		{
			GetWorld()->GetTimerManager().SetTimer(ChangeBalanceTimerHandle,this, &UAdvKitCharacterMovementComponent::ChangeTargetBalance, BalanceChangeTime, false);
		}
	}
}
